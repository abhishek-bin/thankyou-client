import {Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import {TabSelectionsService} from 'src/app/sharing/services/tab-selections.service';
import {NavController} from '@ionic/angular';
import {Router, RouterEvent} from '@angular/router';
import {GlobalService} from '../../services/global.service';

@Component({
    selector: 'app-bottom-tabs',
    templateUrl: './bottom-tabs.component.html',
    styleUrls: ['./bottom-tabs.component.scss']
})
export class BottomTabsComponent implements OnInit {
    selectedPath = '';
    index = 0;
    msgCount = 0;

    constructor(private navCtrl: NavController, private router: Router, private tabService: TabSelectionsService, private globalService: GlobalService
    ) {
        this.router.events.subscribe((event: RouterEvent) => {
            if (event && event.url) {
                this.selectedPath = event.url;
                console.log(event.url);
                if (this.selectedPath.includes('message')) {
                    this.index = 4;
                } else if (this.selectedPath.includes('health')) {
                    this.index = 3;
                } else if (this.selectedPath.includes('createListing')) {
                    this.index = 2;
                } else if (this.selectedPath.includes('non-food')) {
                    this.index = 1;
                } else if (this.selectedPath.includes('food')) {
                    this.index = 0;
                }
            }
        });
    }

    ngOnInit() {
        this.globalService.chatMessageCount.subscribe((message) => {
            // if (message) {
            //     console.log(message);
            // }
            this.msgCount = message;
        });
        this.tabService.$tabSelectedBar.subscribe((res) => {
            // this.tabSelected = res;
            // if (!this.tabSelected) {
            //     this.tabSelected = 'food';
            // }
            if (res) {
                this.index = res.id;
                this.selectedPath = res.selection;
            }
        });
    }

    getSelected(tabSelected: string) {
        this.tabService.updateTabSelected(tabSelected);
    }

    changeItems(tabSelected: string) {
        this.tabService.updateTabSelected(tabSelected);
    }

    async routeTo(route: string, section?: string, index?: number) {
        this.index = index;
        if (section) {
            await this.navCtrl.navigateForward(`/sharing/${route}`, {replaceUrl: true});
            this.changeItems(section);
        } else {
            await this.navCtrl.navigateForward(`/sharing/${route}`, {replaceUrl: true});
        }
    }


}
