import {Component, OnDestroy, OnInit} from '@angular/core';
import {
    NavController,
    ModalController
} from '@ionic/angular';
import {Router} from '@angular/router';
import {NotificationComponent} from '../../../sharing/components/notification/notification.component';
import {AppConstant} from '../../services/app.constant';
import {GlobalService} from '../../services/global.service';
import {Storage} from '@ionic/storage';

// Modals

@Component({
    selector: 'app-my-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
    searchKey = '';
    isHidden = false;
    notificationCount = 0;
    intervalId = 0;
    user: any;
    obs: any;

    constructor(
        public navCtrl: NavController,
        public modalCtrl: ModalController,
        public route: Router,
        public globalService: GlobalService,
        public storage: Storage
    ) {
    }

    async ngOnInit() {
        console.log(this.route.url);
        this.user = await this.storage.get('user');
        this.intervalId = setInterval(() => {
            if (this.route.url !== '/sharing/profile') {
                this.getNotificationCount();
            }
        }, 2000);
    }


    ngOnDestroy() {
        clearInterval(this.intervalId);
        this.obs.unsubscribe();

    }

    async modalShow() {
        const modal = await this.modalCtrl.create({
            component: NotificationComponent
        });
        return await modal.present();
    }


    getNotificationCount() {

        this.obs = this.globalService.getNotificationCount(this.user._id).subscribe((res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                this.notificationCount = res.responseData.notificationCount;
                this.globalService.setCurrentChatCount(res.responseData.messageCount);
            }
        }, (err) => {
            clearInterval(this.intervalId);
        });
    }
}
