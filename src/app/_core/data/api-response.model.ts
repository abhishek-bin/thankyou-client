export class ResponseModel {
    message: string;
    status: number;
    responseData: any;
}
