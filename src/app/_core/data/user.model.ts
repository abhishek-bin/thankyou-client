export class UserModel {
    // initialName: string;
    // firstName: string;
    // middleName: string;
    name: string;
    dob: string;
    email: string;
    phone: string;
    role: number;
    addedOn: number;
    password?: string;
    address: string;
    state: string;
    city: string;
    country: string;
    profileUrl: string;
    location: any;
    userId: string;
    zipCode: string;

    constructor(obj) {
        // this.initialName = obj.initialName;
        // this.firstName = obj.firstName;
        // this.middleName = obj.middleName;
        this.name = obj.name;
        this.zipCode = obj.zipCode;
        this.dob = obj.dob;
        this.email = obj.email;
        this.phone = obj.phone;
        this.role = obj.role;
        this.addedOn = obj.addedOn;
        this.address = obj.address;
        // this.state = obj.state;
        // this.city = obj.city;
        // this.country = obj.country;
        this.profileUrl = obj.profileUrl;
        this.location = obj.location;
        this.userId = obj.userId;
    }
}
