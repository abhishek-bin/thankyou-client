import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HeaderComponent} from '../components/header/header.component';
import {HomeComponent} from 'src/app/sharing/components/home/home.component';
import {LoginComponent} from 'src/app/landing/login/login.component';
import {RegisterComponent} from 'src/app/landing/register/register.component';
import {BottomTabsComponent} from '../components/bottom-tabs/bottom-tabs.component';
import {CustomMaxDirective} from '../directives/custom-max-validation.directive';
import {CustomMinDirective} from '../directives/custom-min-validation.directive';
import {RouterModule} from '@angular/router';
import {SwipeDirective} from '../../swipe/swipe.directive';

@NgModule({
    declarations: [HeaderComponent, HomeComponent, LoginComponent, RegisterComponent, BottomTabsComponent, CustomMaxDirective, CustomMinDirective, SwipeDirective],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    entryComponents: [],

    exports: [
        // CommonModule,
        IonicModule,
        FormsModule,
        HeaderComponent, HomeComponent, LoginComponent, RegisterComponent, BottomTabsComponent, SwipeDirective
    ]
})
export class SharedModule {
}
