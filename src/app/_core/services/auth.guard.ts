import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {GlobalService} from './global.service';
import {from} from 'rxjs';
import {switchMap} from 'rxjs/operators';


@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: GlobalService
    ) {
    }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const token = await this.authenticationService.getToken();
        if (token) {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
        await this.router.navigate(['/']);
        return false;
    }
}
