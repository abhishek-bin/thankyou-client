import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {GlobalService} from './global.service';
import {Router} from '@angular/router';
import {AlertController} from '@ionic/angular';

@Injectable()

export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: GlobalService, private router: Router, private alertController: AlertController) {
    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // @ts-ignore
        return next.handle(request).pipe(catchError(async err => {
            if ( err.status === 403) {
                this.authenticationService.clear().then(async () => {
                    await this.router.navigateByUrl('/');
                    await this.authenticationService.closeLoader();
                });
                return;
            }
            await this.authenticationService.closeLoader();
            // const alert = await this.alertController.create({
            //     subHeader: 'Oops!',
            //     message: err.error.message ? err.error.message : 'Internal server error',
            //     buttons: ['OK'],
            //     // cssClass: 'bg-red'
            // });
            // await alert.present();
            return throwError(err.error);
        }));
    }
}
