import {Injectable} from '@angular/core';
import {HTTP} from '@ionic-native/http/ngx';
import {BehaviorSubject, Observable} from 'rxjs';
import {AppConstant} from './app.constant';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LoadingController} from '@ionic/angular';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})

export class GlobalService {
    token: string;
    loading: any;
    filter: any;
    userObj: any;

    constructor(private loadingController: LoadingController, private storage: Storage, private http: HttpClient) {
    }

    private searchMessage = new BehaviorSubject('');
    // @ts-ignore
    private messageSource = new BehaviorSubject(this.userObj);
    private chatMessageCountSource = new BehaviorSubject(0);

    currentMessage = this.messageSource.asObservable();
    currentSearchMessage = this.searchMessage.asObservable();
    chatMessageCount = this.chatMessageCountSource.asObservable();

    changeMessage(userObj: any) {
        this.messageSource.next(userObj);
    }

    changeSearchMessage(searchMsg) {
        this.searchMessage.next(searchMsg);
    }

    setCurrentFilter(filter): void {
        this.filter = filter;
    }

    getCurrentFilter(): any {
        return this.filter;
    }

    setCurrentChatCount(count): any {
        this.chatMessageCountSource.next(count);
    }

    setToken(token: string): void {
        this.storage.set('bearerToken', token);
    }

    clear(): any {
        return this.storage.clear();
    }

    getToken(): any {
        return this.storage.get('token');
    }

    getProductByUserId(obj) {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.PRODUCT_BY_ME}/${obj.addedBy}/${obj.productId}`);
    }

    saveDeviceToken(obj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.SAVE_TOKEN}`, obj);
    }

    login(reqObj): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.LOGIN}`, reqObj);
    }

    register(reqObj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.REGISTER}`, reqObj);
    }

    markReadMessages(id): Observable<any> {
        return this.http.put(`${AppConstant.BASE_URL}${AppConstant.READ_MESSAGE}/${id}`, {});
    }

    fbLogin(reqObj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.FB_LOGIN}`, reqObj);
    }

    gmailLogin(reqObj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.GMAIL_LOGIN}`, reqObj);
    }

    donate(reqObj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.DONATE}`, reqObj);
    }

    deleteChat(reqObj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.DELETE_CHAT}`, reqObj);
    }

    want(reqObj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.WANT}`, reqObj);
    }

    getUsersNearMe(location, id): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.USER_NEAR_ME}/${id}/?${AppConstant.LOCATION}=${location}`);
    }

    getUserById(userId): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.PUBLIC_USER}/${userId}`);
    }

    getProduct(productId?): Observable<any> {
        if (productId) {
            return this.http.get(`${AppConstant.BASE_URL}${AppConstant.PRODUCT}/${productId}`);

        } else {
            return this.http.get(`${AppConstant.BASE_URL}${AppConstant.PRODUCT}`);

        }

    }

    getRelatedProduct(productId, category): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.RELATED_PRODUCT}/${category}/${productId}`);
    }

    updateProduct(str): Observable<any> {
        return this.http.put(`${AppConstant.BASE_URL}${AppConstant.PRODUCT}/${str}`, {});
    }

    getProductByParam(userId, location): Observable<any> {
        console.log(location);
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.PRODUCT}/${AppConstant.USER}`, {
            userId,
            location: [location.coords.longitude, location.coords.latitude]
        });
    }

    getMyProduct(userId): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.MY_PRODUCT}/${userId}`);
    }

    getBlog(blogId?): Observable<any> {
        if (blogId) {
            return this.http.get(`${AppConstant.BASE_URL}${AppConstant.BLOG}?blogId=${blogId}`);

        } else {
            return this.http.get(`${AppConstant.BASE_URL}${AppConstant.BLOG}`);

        }

    }

    getChatUserList(userId): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.CHAT_HISTORY}/${userId}`);
    }

    logout(userId): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.LOGOUT}/${userId}`, {});
    }

    getMessageList(senderId, receiverId, epochTime): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.MESSAGE}/${senderId}/${receiverId}/${epochTime}`);
    }

    sendMessage(req): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.MESSAGE}`, req);
    }

    // uploadMedia(req): Observable<any> {
    //     return this.http.post(`${AppConstant.BASE_URL}${AppConstant.UPLOAD_MEDIA}`, req);
    // }

    updateUser(req): Observable<any> {
        return this.http.put(`${AppConstant.BASE_URL}${AppConstant.USER}`, req);
    }

    sendOtp(phone): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.SEND_OTP}/${phone}`);
    }

    resetOtp(phone): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.RESET_OTP}/${phone}`);
    }

    verifyOtp(obj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.VERIFY_OTP}`, obj);
    }

    resetPassword(obj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.RESET_PASSWORD}`, obj);
    }

    needHelp(obj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.NEED_HELP}`, obj);
    }

    feedback(obj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.FEEDBACK}`, obj);
    }

    requestListing(pId): Observable<any> {
        return this.http.put(`${AppConstant.BASE_URL}${AppConstant.REQ_LISTING}/${pId}`, {});
    }

    likeProduct(pId): Observable<any> {
        return this.http.put(`${AppConstant.BASE_URL}${AppConstant.LIKE_PRODUCT}/${pId}`, {});
    }

    likeBlog(pId): Observable<any> {
        return this.http.put(`${AppConstant.BASE_URL}${AppConstant.LIKE_BLOG}/${pId}`, {});
    }

    commentBlog(obj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.COMMENT_POST}`, obj);
    }

    addDonation(obj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.DONATION}`, obj);
    }

    getDonation(donationId?): Observable<any> {
        if (donationId) {
            return this.http.get(`${AppConstant.BASE_URL}${AppConstant.DONATION}?donationId=${donationId}`);

        } else {
            return this.http.get(`${AppConstant.BASE_URL}${AppConstant.DONATION}`);
        }
    }

    addPaymentStatus(obj): Observable<any> {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.PAYMENT_STATUS}`, obj);
    }

    getNotification(userId): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.NOTIFICATION}/${userId}`);
    }

    getNotificationCount(userId): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.NOTIFICATION_COUNT}/${userId}`);
    }

    markReadNotification(userId): Observable<any> {
        return this.http.put(`${AppConstant.BASE_URL}${AppConstant.NOTIFICATION}/${userId}`, {});
    }

    rateProduct(obj): Observable<any> {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.RATE}/${obj.productId}/${obj.value}`, {});
    }


    async showLoader() {
        this.loading = await this.loadingController.create({
            message: ''
        });
        await this.loading.present();
    }

    async closeLoader() {
        if (this.loading) {
            await this.loading.dismiss();
        }
    }

    sendChatRequest(req) {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.CHAT_REQUEST}`, req);

    }

    getChatRequestStatus(req: any) {
        return this.http.get(`${AppConstant.BASE_URL}${AppConstant.CHAT_REQUEST}/${req.senderId}/${req.receiverId}`);

    }

    rejectRequest(userId) {
        return this.http.delete(`${AppConstant.BASE_URL}${AppConstant.CHAT_REQUEST}/${userId}`, {});
    }

    acceptRequest(userId) {
        return this.http.put(`${AppConstant.BASE_URL}${AppConstant.CHAT_REQUEST}/${userId}`, {});
    }

    blockUser(receiverId) {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.BLOCK}/${receiverId}`, {});

    }

    unBlockUser(receiverId) {
        return this.http.put(`${AppConstant.BASE_URL}${AppConstant.UNBLOCK}/${receiverId}`, {});

    }

    reportUser(receiverId: any) {
        return this.http.post(`${AppConstant.BASE_URL}${AppConstant.REPORT}/${receiverId}`, {});

    }
}
