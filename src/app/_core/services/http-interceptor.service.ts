import {Injectable} from '@angular/core';
// import { ErrorDialogService } from '../error-dialog/errordialog.service';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
} from '@angular/common/http';

import {from, Observable} from 'rxjs';
import {GlobalService} from './global.service';
import {Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
    constructor(private global: GlobalService, private router: Router) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (this.router.routerState.snapshot.url !== '/' && this.router.routerState.snapshot.url !== '/landing/register') {
            return from(this.global.getToken())
                .pipe(
                    switchMap(token => {
                        const headers = request.headers
                            .set('x-access-token', '' + token)
                            .append('Content-Type', 'application/json');
                        const requestClone = request.clone({
                            headers
                        });
                        return next.handle(requestClone);
                    })
                );
        } else {
            return next.handle(request);
        }
    }
}
