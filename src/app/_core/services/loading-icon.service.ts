import {Injectable} from '@angular/core';
import {LoadingController} from '@ionic/angular';

@Injectable()
export class LoadingIconService {
    loading: any;

    constructor(public loadingController: LoadingController) {
        this.LoadingInitialize();
    }

    async LoadingInitialize() {
        this.loading = await this.loadingController.create({});
    }

    LoadingOn() {
        this.loading.present();
    }

    LoadingOff() {
        setTimeout(async () => {
            this.loading.dismiss();
        }, 500);
    }
}
