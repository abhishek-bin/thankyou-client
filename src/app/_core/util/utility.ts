//import * as moment from 'moment';
import {ResponseModel} from '../data/api-response.model';

export class Utility {

    /// <summary>
    /// Alpha numeric keycode collection
    /// </summary>
    static alphaNumericKeycode: number[] = [
        8, 13, 17, 32, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
        65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, , 86, 87, 88, 89, 90,
        97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122
    ];

    /// <summary>
    /// This function will replace whitespace character in the string
    /// </summary>
    /// <param name="word"></param>
    /// <returns>string</returns>
    static replaceWhitespace = (word: string) => {
        return word.replace(/ /g, '');
    };

    /// <summary>
    /// This function will check special characters keycode
    /// </summary>
    /// <param name="keyCode"></param>
    /// <returns>boolean</returns>
    static checkSpecialCharacter = (keyCode: number) => {
        return Utility.alphaNumericKeycode.indexOf(keyCode) > -1 ? false : true;
    };

    /// <summary>
    /// This function will validate alpha character in string
    /// </summary>
    /// <param name="word"></param>
    /// <returns>boolean</returns>
    static isValidString = (word: string) => {
        var reg = /[^a-zA-Z]/;
        return reg.test(word) ? true : false;
    };

    /// <summary>
    /// This function will validate date and time
    /// </summary>
    /// <param name="date"></param>
    /// <returns>boolean</returns>
    static isValidDate = (date: any) => {
        return date != null && !isNaN(Date.parse(date)) ? true : false;
    };

    /// <summary>
    /// This function will return Hour:Min:Sec  format between two dates
    /// </summary>
    /// <param name="endDate"></param>
    /// <param name="startDate"></param>
    /// <returns>string</returns>
    // static getHoursMinutesSeconds = (endDate: Date, startDate: Date) => {
    //   let totalTime = "0";
    //   if (endDate && startDate) {
    //     let endDateTime = new Date(moment(endDate).seconds(0).milliseconds(0).toISOString());
    //     let startDateTime = new Date(moment(startDate).seconds(0).milliseconds(0).toISOString());
    //     let calculateTime = (endDateTime.getTime() - startDateTime.getTime()) / 1000;
    //     if (calculateTime > 0) {
    //       var hours = Math.floor(calculateTime / 3600);
    //       var minutes = Math.floor((calculateTime - (hours * 3600)) / 60);
    //       var seconds = calculateTime - (hours * 3600) - (minutes * 60);
    //       var _hours = hours.toString();
    //       var _minutes = minutes.toString();
    //       var _seconds = seconds.toString();
    //       if (hours < 10) {
    //         _hours = "0" + _hours;
    //       }
    //       if (minutes < 10) {
    //         _minutes = "0" + _minutes;
    //       }
    //       if (seconds < 10) {
    //         _seconds = "0" + _seconds;
    //       }
    //       totalTime = _hours + ":" + _minutes + ":" + Math.floor(parseInt(_seconds));
    //     }
    //     else {
    //       totalTime = "0";
    //     }
    //     return totalTime;
    //   }
    //   return "0";
    // }

    /// <summary>
    /// This function will return Hour:Min  format from total minutes
    /// </summary>
    /// <param name="endDate"></param>
    /// <param name="startDate"></param>
    /// <returns>string</returns>
    static getHoursAndMinutesByTotalMin = (calculateTime: number) => {
        let totalTime = '0';
        if (calculateTime > 0) {
            var hours = Math.floor(calculateTime / 60);
            var minutes = Math.floor((calculateTime - (hours * 60)));
            var _hours = hours.toString();
            var _minutes = minutes.toString();
            if (hours < 10) {
                _hours = '0' + _hours;
            }
            if (minutes < 10) {
                _minutes = '0' + _minutes;
            }
            totalTime = _hours + ':' + _minutes;
        } else {
            totalTime = '0';
        }
        return totalTime;
    };


    /// <summary>
    /// This function will return Hour:Min  format between two dates
    /// </summary>
    /// <param name="endDate"></param>
    /// <param name="startDate"></param>
    /// <returns>string</returns>
    // static getHoursAndMinutes = (endDate: Date, startDate: Date) => {
    //   let totalTime = "0";
    //   if (endDate && startDate) {
    //     let endDateTime = new Date(moment(endDate).seconds(0).milliseconds(0).toISOString());
    //     let startDateTime = new Date(moment(startDate).seconds(0).milliseconds(0).toISOString());
    //     let calculateTime = (endDateTime.getTime() - startDateTime.getTime()) / 1000;
    //     if (calculateTime > 0) {
    //       var hours = Math.floor(calculateTime / 3600);
    //       var minutes = Math.floor((calculateTime - (hours * 3600)) / 60);
    //       var _hours = hours.toString();
    //       var _minutes = minutes.toString();
    //       if (hours < 10) {
    //         _hours = "0" + _hours;
    //       }
    //       if (minutes < 10) {
    //         _minutes = "0" + _minutes;
    //       }
    //       totalTime = _hours + ":" + _minutes;
    //     }
    //     else {
    //       totalTime = "00:00";
    //     }
    //     return totalTime;
    //   } else {
    //     return "00:00";
    //   }

    // }
    /// <summary>
    /// This function will return Min format between two dates
    /// </summary>
    /// <param name="endDate"></param>
    /// <param name="startDate"></param>
    /// <returns>number</returns>
    // static getMinutes = (endDate: Date, startDate: Date) => {
    //   let totalMinutes = 0;
    //   if (endDate && startDate) {
    //     let endDateTime = new Date(moment(endDate).seconds(0).milliseconds(0).toISOString());
    //     let startDateTime = new Date(moment(startDate).seconds(0).milliseconds(0).toISOString());
    //     let calculateTime = (endDateTime.getTime() - startDateTime.getTime()) / 1000;
    //     if (calculateTime > 0) {
    //       totalMinutes = calculateTime / 60;
    //     }
    //     else {
    //       totalMinutes = 0;
    //     }
    //     return totalMinutes;
    //   }
    //   return 0;
    // }


    /// <summary>
    /// This function will return Text from Drop Down
    /// </summary>
    /// <param name="Any"></param>
    /// <param name="Value"></param>
    /// <param name="Type"></param>
    /// <returns>text</returns>
    static getSelectedText(arr: any, val: any, type?: string) {
        let selectedText = '';
        if (arr && arr.length > 0) {
            if (val != null && val != undefined) {
                let newArr = arr.filter(a => a.Name == val);
                if (newArr.length > 0) {
                    if (type === 'copy') {
                        selectedText = newArr[0].Name;
                    } else {
                        selectedText = newArr[0].Value;
                    }
                }
            }
        }
        return selectedText;
    }

    /// <summary>
    /// This function will parse json into object and then made as error string profile
    /// </summary>
    /// <param name="error"></param>
    /// <returns>ResponseModel</returns>
    // static processErrorMessage = (error) => {
    //   let errorCollections="";
    //   try {
    //     if(typeof(error) == 'object'){
    //       errorCollections = error.Message;
    //       if(error.ValidationErrors && error.ValidationErrors.length>0){
    //         errorCollections+=": ";
    //       }
    //       error.ValidationErrors.forEach(function (value) {
    //         errorCollections += value.Message
    //        });
    //     }
    //   else {
    //     var index = error.indexOf('{');
    //     var errorResponse = error.substr(index, error.length);
    //     let respModel = <ResponseModel>JSON.parse(errorResponse);
    //     errorCollections = respModel.Message;
    //     if(respModel.ValidationErrors && respModel.ValidationErrors.length>0){
    //       errorCollections+=": ";
    //     }
    //     respModel.ValidationErrors.forEach(function (value) {
    //       errorCollections += value.Message
    //      });
    //    }
    //   }
    //    catch (error) {
    //         errorCollections = "Error: While parsing the error response."
    //     }
    //     return errorCollections;
    //   }


    /// <summary>
    /// This function will return leading zero number
    /// </summary>
    /// <param name="number">number</param>
    /// <param name="number">places</param>
    /// <returns>number</returns>
    static zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join('0') + num;
    }

    /// <summary>
    /// This function will create a deep copy of object or array of object.
    /// </summary>
    /// <param name="oldObj"></param>
    /// <returns>newObj</returns>
    static deepCopy(oldObj: any) {
        var newObj = oldObj;
        if (oldObj && typeof oldObj === 'object') {
            newObj = Object.prototype.toString.call(oldObj) === '[object Array]' ? [] : {};
            for (var i in oldObj) {
                newObj[i] = this.deepCopy(oldObj[i]);
            }
        }
        return newObj;
    }

    /// <summary>
    /// This function will return Yesterday Date only
    /// </summary>
    static getYesterdayDate(): string {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        let _month = (date.getMonth() + 1).toString();
        let _date = date.getDate().toString();
        if (parseInt(_month) < 10) {
            _month = '0' + _month;
        }
        if (parseInt(_date) < 10) {
            _date = '0' + _date;
        }
        return date.getFullYear() + '/' + _month + '/' + _date;
    }

    /// <summary>
    /// This function will return Date Part only in MM/DD/YYYY format
    /// </summary>
    static getDatePart(date: any): string {
        let get_Date = '';
        if (date != null && !isNaN(Date.parse(date))) {
            var d = new Date(date);
            let _month = (d.getMonth() + 1).toString();
            let _date = d.getDate().toString();
            if (parseInt(_month) < 10) {
                _month = '0' + _month;
            }
            if (parseInt(_date) < 10) {
                _date = '0' + _date;
            }
            get_Date = _month + '/' + _date + '/' + d.getFullYear();
        }
        return get_Date;
    }

    /// <summary>
    /// This function will return Date Part only in MM-DD-YYYY format
    /// </summary>
    static getDatePartInDashFormat(date: any): string {
        let get_Date = '';
        if (date != null && !isNaN(Date.parse(date))) {
            var d = new Date(date);
            let _month = (d.getMonth() + 1).toString();
            let _date = d.getDate().toString();
            if (parseInt(_month) < 10) {
                _month = '0' + _month;
            }
            if (parseInt(_date) < 10) {
                _date = '0' + _date;
            }
            get_Date = _month + '-' + _date + '-' + d.getFullYear();
        }
        return get_Date;
    }

    /// <summary>
    /// This function will get current Date only
    /// </summary>
    static getCurrentDate(): string {
        var date = new Date();
        let _month = (date.getMonth() + 1).toString();
        let _date = date.getDate().toString();
        if (parseInt(_month) < 10) {
            _month = '0' + _month;
        }
        if (parseInt(_date) < 10) {
            _date = '0' + _date;
        }
        return date.getFullYear() + '/' + _month + '/' + _date;
    }

    /// <summary>
    /// This method is created to convert current machine date and time to UTC format.
    /// </summary>
    /// <returns></returns>
    static dateToLocale(_date: any) {
        // if(_date){
        //   if(_date.indexOf('Z')>-1 ) {
        //     _date = _date.substr(0,_date.length-1);
        //   }
        //   _date = new Date(new Date(_date).toLocaleString("en-US").toString() + " UTC").toString();
        // }
        return _date;
    }

    /// <summary>
    /// This method is created to convert API response date to Local machine time format GMT.
    /// </summary>
    /// <returns></returns>
    static dateToUTC(_date: any) {
        // if(_date){
        //   _date = new Date(_date).toUTCString();
        // }
        return _date;

    }

    /// <summary>
    /// This method is used to override the format of standard format(OWL_MOMENT_DATE_TIME_FORMATS).
    /// </summary>
    /// <returns></returns>
    static MY_MOMENT_FORMATS = {
        parseInput: 'LL LT',
        fullPickerInput: 'MM/DD/YYYY HH:mm', /* Here we can override the format */
        datePickerInput: 'LL',
        timePickerInput: 'LT',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MM YYYY',
    };

}
