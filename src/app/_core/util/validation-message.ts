export const COMMON = {
    SPECIALCHAR_VALIDATION_MSG: "Special character is not allowed",
    SPACE_VALIDATION_MSG: "COMMON_SPACE_VALIDATION_MSG",
    EMPTY_FIELD_MSG: "COMMON_EMPTY_FIELD_MSG",
    DATE_VALIDATION: "COMMON_DATE_VALIDATION",
    EMPTY_VALIDATION: " can not be empty.",
    STARTDATE_ENDDATE_COMPARE: "COMMON_STARTDATE_ENDDATE_COMPARE",
    FILTER_FIELD_MSG: "COMMON_FILTER_FIELD_MSG",
    START_DATE_MUST_BE_LESS:"COMMON_START_DATE_MUST_BE_LESS",
    VALID_START_DATE:"VALID_START_DATE",
    VALID_END_DATE:"VALID_END_DATE",
    RECORD_ALREADY_SELECTED:"RECORD_ALREADY_SELECTED",
    DUPLICATE_CODE: "DUPLICATE_CODE",
    MIN_MAX_COMPARE:"MIN_MAX_COMPARE"
};


export const CUSTOM_DICTIONARY = {
    LENGTH_MSG: "CUSTOM_DICTIONARY_LENGTH_MSG",
    CUSTOM_DICTIONARY_INSERT_MSG: "CUSTOM_DICTIONARY_INSERT_MSG",
    CUSTOM_DICTIONARY_UPDATE_MSG: "CUSTOM_DICTIONARY_UPDATE_MSG",
    CUSTOM_DICTIONARY_DELETE_MSG: "CUSTOM_DICTIONARY_DELETE_MSG",
};
export const WEB_LINK = {
    WEB_LINK_CREATE_MSG: "WEB_LINK_CREATE_MSG",
    SELECT_USER: "WEB_LINK_SELECT_USER",
    PEND_MSG : "WEB_LINK_PEND_MSG"
}

export const COMMUNICATION_HISTORY = {
    DATA_PRESENCE : "DATA_PRESENCE",
    COMM_HISTORY_MESSAGE_LENGTH : "Message Text has maximum of 250 characters.",
}

export const PROBLEM_CODE_CREATION = {
    LENGTH_MSG: "Code length must be 2 Characters",
    PROBLEM_CODE_INSERT_MSG: "PROBLEM_CODE_INSERT_MSG",
    PROBLEM_CODE_UPDATE_MSG: "PROBLEM_CODE_UPDATE_MSG",
    PROBLEM_CODE_DELETE_MSG: "Code Successfully Deleted",
    PROBLEM_CODE_UPDATE_FAILED: "PROBLEM_CODE_UPDATE_FAILED",
    MAXIMUM_TABS_SELECT_MSG: "MAXIMUM_TABS_SELECT_MSG",
};
export const NOTIFICATION_CODE = {
    LENGTH_MSG: "Only 50 characters allowed.",
    INSERT_MSG: "NOTIFICATION_CODE_INSERT_MSG",
    UPDATE_MSG: "NOTIFICATION_CODE_UPDATE_MSG",
    DELETE_MSG: "NOTIFICATION_CODE_DELETE_MSG",
    MAXIMUM_LIMIT: "NOTIFICATION_CODE_MAXIMUM_LIMIT"
}
export const FILE_ATTACHMENT = {
    DELETE_ATTACHMENT: "IF_File_Attachment_Uploading_DELETE_ATTACHMENT ",
    UPLOAD_ATTACHEMT: "IF_File_Attachment_Uploading_UPLOAD_ATTACHEMT",
    FILE_NOT_ALLOWED_WITHOUT_EXTENSION: " FILE_NOT_ALLOWED_WITHOUT_EXTENSION",
    FILE_NOT_ALLOWED_EXTENSION: "FILE_NOT_ALLOWED_EXTENSION",
    FILE_MAXSIZE: "FILE_MAXSIZE",
    FILE_ATTEMP_VALIDATION: "FILE_ATTEMP_VALIDATION",
    FILE_MAXIMUM_FILE_ALLOWED: "FILE_MAXIMUM_FILE_ALLOWED",
    FILE_REMOVE: " Single row can not be removed. ",
    FILE_NOT_ALLOWED_NEW_FILE_WITHOUT_PREVIOUS_FILE_SELECT: "Please select previous file.",
    FILE_DUPLICATE_NOT_ALLOWED: "FILE_DUPLICATE_NOT_ALLOWED",
    FILE_UPLOADING_ERROR:"FILE_UPLOADING_ERROR",
    FILE_URL_MISSING:"FILE_URL_MISSING",
   
    FILE_ATTACHMENT_UPLOADING_MB:"IF_File_Attachment_Uploading_MB",
    FILE_ATTACHMENT_UPLOADING_MB_OUT_OF:"IF_File_Attachment_Uploading_MB_Out_Of"
};
export const CANCEL_ADD_TRIP = {
  TRANSITDATE_IS_REQUIRED:"Transit Date is required.",
  ROUTE_IS_REQUIRED:"Route is required.",
  BLOCKID_IS_REQUIRED:"Block ID is required.",
  NO_TRIP_FOUND :"No Trip Found.",
  STARTTIME_IS_REQUIRED:"Start Time is required.",
  ENDTIME_IS_REQUIRED:"End Time is required.",
  DIRECTION_IS_REQUIRED:"Direction is required.",
  START_TP_IS_REQUIRED:"Start TP is required.",
  END_TP_IS_REQUIRED:"End TP is required.",
  VARIANT_IS_REQUIRED:"Variant is required.",
  RUN_NUMBER_IS_REQUIRED:"Run Number is required.",
  STARTDATE_IS_LESS_THEN_ENDDATE : "STARTDATE_IS_LESS_THEN_ENDDATE",
  STARTDATE_IS_GREATER_THEN_TRANSIT_STARTDATE : "STARTDATE_IS_GREATER_THEN_TRANSIT_STARTDATE",
  ENDDATE_IS_LESS_THEN_TRANSIT_ENDDATE : "ENDDATE_IS_LESS_THEN_TRANSIT_ENDDATE",
  ENDDATE_IS_BEFORE_OR_EQUAL_TO_STARTDATE_OF_NEXT_TRIP : "ENDDATE_IS_BEFORE_OR_EQUAL_TO_STARTDATE_OF_NEXT_TRIP",
  STARTDATE_IS_GREATER_OR_EQUAL_TO_ENDDATE : "STARTDATE_IS_GREATER_OR_EQUAL_TO_ENDDATE",
  ENDDATE_IS_BEFORE_OR_EQUAL_TO_STARTDATE_OF_PREVIOUS_TRIP : "ENDDATE_IS_BEFORE_OR_EQUAL_TO_STARTDATE_OF_PREVIOUS_TRIP",
  STARTPOINT_IS_EQUAL_TO_ENDPOINT_OF_PREVIOUS_TRIP : "STARTPOINT_IS_EQUAL_TO_ENDPOINT_OF_PREVIOUS_TRIP",
  NO_CHANGE_DETECTED : "NO_CHANGE_DETECTED",
  STARTDATE_IS_AFTER_CURRENTDATE : "STARTDATE_IS_AFTER_CURRENTDATE",
  TRIP_CANNOT_CANCEL_THAT_HAS_ENDED : "TRIP_CANNOT_CANCEL_THAT_HAS_ENDED",
  TRIP_ONHOVER_ERROR_DETAILS : "Note: On Hover to the row for error details.",
  TRIP_INFO_RELOAD :"TRIP_INFO_RELOAD",
  DELETE_ALL_TRIPS :"DELETE_ALL_TRIPS"
}
export const DATA_MESSAGES = {
    UNCHANGED_VALUE: "UNCHANGED_VALUE",
    MESSAGE_COUNT:"MESSAGE_COUNT"
}

export const INCIDENT_FORM={
    SELECT_INCIDENT_TYPE:"SELECT_INCIDENT_TYPE",
    SELECT_INCIDENT_CODE: "SELECT_INCIDENT_CODE",
    SELECT_ASSOCIATE_FORM:"Please select incident form.",
    DUPLICATE_ASSOCIATE_FORM :"Duplicate associate form.",
    ASSOCIATE_SUCCESSFULLY:"Form associated successfully.",
    DATE_VALIDATION:"Date_Validation",
}

export const MANAGE_USERS = {
    NO_RECORD: "No_Records_Found",
    ENTER_VALUE:"ENTER_VALUE",
    PASSWORD_NOT_SAME:"PASSWORD_NOT_SAME",
    PASSWORD_LENGTH:"PASSWORD_LENGTH"
}

export const VCS_REPORT_CONFIGURATION_CATALOG = {
    NO_TARGET_TYPE: "No target types records found.",
    NO_TARGET:"No targets records found.",
    NO_TARGET_PACKAGES:"No targets packages found.",
    CHOOSE_RELEASE:"Please choose a release version first."
}
export const VCS_GROUP_SECTION = {
    PACKAGE_LOAD_FAILED: "Loading package parameters is failed!",
    NO_GROUP_FOUND: "There are no Groups to show in database!",
    GROUP_ASSOCIATED_PARAMETER: "This group is associated with particular parameters, you are not allowed to delete it.",
    GROUP_CONTAINS_SECTIONS: "The group cannot be deleted. Please delete all sections in the group first.",
    GROUP_DELETED: "New group is deleted successfully!",
    GROUP_NOT_DELETED: "New group is not deleted successfully!",
    DUPLICATE_GROUP: 'Another group exists with same name.',
    GROUP_ADDED: "New group is added successfully!",
    GROUP_NOT_ADDED:"New group is not added successfully!",
    SECTION_ASSOCIATED_PARAMETER: 'The selected sections are associated with particular parameters, you are not allowed to delete it.',
    SECTION_DELETED: "Section is deleted successfully!",
    SECTION_NOT_DELETED: "Section is not deleted successfully!",
    DUPLICATE_SECTION: 'Another section exists with same name.',
    SECTION_ADDED: "New section is added successfully!",
    SECTION_NOT_ADDED: "New section is not added successfully!",
    PARAMETERCREATIONFAIL: "There is no data came from Parameter Creation API Endpoint!"
}

export const CATALOG_VEHICLE_GROUP = {
    GROUP_ADDED: "Group Added Successfully!",
    GROUP_NOT_ADDED:"Group cannot be added!",
    DUPLICATE_GROUP:"Already Recurred!", 
    GROUP_DELETED: "Group Deleted Successfully!",
    GROUP_NOT_DELETED:"Group cannot be deleted!", 
    NO_SPACE_GROUP_NAME:"Space not allowed",
    NO_GROUP_DATA:"No Group Data",
    GROUP_NAME_REQUIRED:"Group Name Required",
    VEHICLE_ASSIGNED:"Vehicles Assigned Successfully",
    ERROR_VEHICLE_ASSIGNED:"Vehicles Failed to be Assigned",
    VEHICLE_AVAILABLE:"Vehicles Deleted Successfully",
    ERROR_VEHICLE_AVAILABLE:"Vehicles Failed to be Deleted"
}

export const VCS_REPORT_COMPARISON_RELEASE ={
    TARGET_TYPE_ERROR: "VCRelease Failed.",
    SEARCH_DATA_ERROR: "Error in Search.",
    CHOOSE_RELEASE : "Please choose second release version first."
}

export const VCS_ADMIN_PARAMETER ={
    PARAMETER_DELETE_SUCCESS : "Parameter deleted successfully.",
    PARAMETER_DELETE_ERROR : "Something went wrong, unable to delete.",
    IMPORT_SUCCESSFULLY : "Parameters imported successfully.",
    VHM_FILE_IMPORT_SUCCESSFULLY : "VHM Files imported successfully",
    INCORRECT_FILE_TYPE : "Incorrect file type. File type should be XML.",
    PARAMETER_NAME_REQUIRED : "Parameter name is required.",
    DEFAULT_VALUE_REQUIRED : "Default value is required."
}

export const VCS_CUSTOM_VEHICLE_GROUP = {
    GROUP_ADDED: "Group Added Successfully!",
    GROUP_NOT_ADDED:"Group cannot be added!",
    DUPLICATE_GROUP:"Already Recurred!", 
    GROUP_DELETED: "Group Deleted Successfully!",
    GROUP_NOT_DELETED:"Group cannot be deleted!", 
    NO_SPACE_GROUP_NAME:"Space not allowed",
    NO_GROUP_DATA:"No Group Data",
    GROUP_NAME_REQUIRED:"Group Name Required",
    VEHICLE_ASSIGNED:"Vehicles Assigned Successfully",
    ERROR_VEHICLE_ASSIGNED:"Vehicles Failed to be Assigned",
    VEHICLE_AVAILABLE:"Vehicles Deleted Successfully",
    ERROR_VEHICLE_AVAILABLE:"Vehicles Failed to be Deleted"
}

export const VCS_THEME_DISPLAY = {
    THEME_ADDED: "Updated Successfully!",
}
export const VCS_UNASSINED_CONFIG_PARAMETER = {
    PACKAGE_PARAMETER_NOT_Found:"Package parameters could not be found!",
    NO_PARAMETER_RECORDS:"No records found with this parameter!"
}

export const VCS_AUDIO_SETTING ={
    AUDIO_SETTING_ITEMS_NOT_FOUND : "No audio settings found for above selections.",
    AUDIO_SETTING_UPDATE_SUCCESS : "Audio Settings saved successfully.",
    AUDIO_SETTING_UPDATE_FAIL : "Audio Settings Failed to be updated.",
    NOTHING_TO_UPDATE : "Nothing to update."

}

export const VCS_SETUP_DIAGNOSTICS = {
    SETUP_DIAGNOSTICS_PASSWORD_REQUIRED : "Password is required.",
    SETUP_DIAGNOSTICS_NUMBER_ONLY:"passcode should contain numbers only.",
    SETUP_DIAGNOSTICS_MAX_LENGTH : "Passcode length can not exceed ",
    SETUP_DIAGNOSTICS_MIN_LENGTH : "Passcode must be atleast ",
    SETUP_DIAGNOSTICS_CHARACTERS : " characters",
    SETUP_DIAGNOSTICS_CHARACTER_LONG : " character long."
}

export const DISPLAY_SETTING = {
    NOTHING_TO_UPDATE : 'Nothing To Update.'
}

export const VCS_IMPORT ={
    VCS_IMPORT_FILE_MSG:'VCS Config file importing is in progress, it will take some time. Please stay on the same screen till the process will complete.',
    VCS_IMPORT_SIGNALR_RESPONSE :'Click here for VCS Data import details.'
}

export const VOIP_MESSAGES = {
    NO_RECCORDS: "No Records found in database."
}

export const VOIP_MAINTENANCE = {
    NO_MDN: "NO_MDN",
    NO_ASSIGNMENT_LIST: "NO_ASSIGNMENT_LIST",
    NO_DATA_REFERENCE_LIST: "NO_DATA_REFERENCE_LIST",
    VoIP_CREATED: "VoIP_CREATED",
    VoIP_NOT_CREATED: "VoIP_NOT_CREATED",
    VoIP_UPDATED: "VoIP_UPDATED",
    VoIP_NOT_UPDATED: "VoIP_NOT_UPDATED",
    VoIP_DELETED: "VoIP_DELETED",
    VoIP_NOT_DELETED: "VoIP_NOT_DELETED"
}

export const VEHICLE_LISTING = {
    DUPLICATE_ID: "Vehicle id already exists, Please enter an unique id.",
    VEHICLE_ID_RANGE: "The Vehicle ID must be between 1 and 65000."
};

export const VEHICLE_MECHANICAL_ALARM = {
    NO_GEOGRAPHIC_AREA: "No processed Geographical area list found.",
    NO_MECHANICAL_ALARM: "No Vehicle Mechanical Alarm found.",
    MECHANICAL_ALARM_CREATED: "Vehicle Mechanical Alarm is successfully created.",
    MECHANICAL_ALARM_NOT_CREATED: "Vehicle Mechanical Alarm is not created.",
    MECHANICAL_ALARM_UPDATED: "Vehicle Mechanical Alarm is successfully updated.",
    MECHANICAL_ALARM_NOT_UPDATED: "Vehicle Mechanical Alarm is not updated.",
    MECHANICAL_ALARM_NOT_DELETED: "Vehicle Mechanical Alarm is not deleted.",
    MECHANICAL_ALARM_DELETED: "Vehicle Mechanical Alarm is successfully deleted."
}

export const INCIDENT_VIEW = {
    START_DATE:"IF_Incident_View_Validate_Start_Date",
    EMPTY_START_DATE:"IF_Incident_View_Empty_Start_Date",
    END_DATE:"IF_Incident_View_Validate_End_Date",
    EMPTY_END_DATE:"IF_Incident_View_Empty_End_Date",
    SERVICE_INTERRUPTION:"IF_Incident_view_Service_Interruption",
    EXTRA_SERVICE:"IF_Incident_View_Extra_Service",
    NOT_AUTHORIZED:"IF_Incident_View_Not_Authorized",
    LOADING_ERROR:"IF_Incident_View_Loading_Error"
}

export const NOTIFICATION_LIST = {
    EMPTY_VALIDATION:"IF_Notification_List_Empty_Validation"
}

export const RELAY_BUMP = {
    PROJECT_DATE_VALIDATION:"IF_Relay_Bump_Validate_Project_In_Service",
    START_DATE_TIME:"IF_Relay_Bump_Start_Date_Time",
    END_DATE_TIME:"IF_Relay_Bump_End_Date_Time",
    AT_TIME:"IF_Relay_Bump_At_Time",
    SCHEDULE_TIME:"IF_Relay_Bump_Schedule_Time"
}

export const INCIDENT_SEARCH = {
    EMPTY_START_DATE:"IF_Incident_Search_Empty_Start_Date",
    EMPTY_END_DATE:"IF_Incident_Search_Empty_End_Date",
    EMPTY_SEARCH_TEXT:"IF_Incident_Search_Empty_Search_Text"
}

export const ACCIDENT_INFO_DRIVER = {
    FIELD_VALIDATION:"IF_Accident_Info_Driver_message"
}

export const CODE_ASSIGNMENT = {
    INITIAL_ASSIGNMENT:"IF_Code_Assignment_Validation_Initial_Assignment",
    FINAL_ASSIGNMENT:"IF_Code_Assignment_Validation_Final_Assignment"
}

export const DEADHEAD_TURNAROUND = {
    PROJECT_DATE:"IF_Deadhead_Validation_Project_Service_Time",
    ACTUAL_DATE:"IF_Deadhead_Validation_Actual_Service_Time",
    SCHEDULE_DATE:"IF_Deadhead_Validation_Scheduled_Time"
}

export const ELECTRONIC_MALFUNCTION = {
    CHECKED_DATE:"IF_Electronic_Mal_Func_Checked_Date",
    REPAIRED_DATE:"IF_Electronic_Mal_Func_Repaired_Date"
}

export const PROPERTY_TRANSLATION = {
    Validation_Combination:"IF_Property_Translation_Validation_Combination"
}

export const COPY_EXISTING_TEMPLATE = {
    ALREADY_EXSITS:"COPY_EXISTING_TEMPLATE_ALREADY_EXSITS",
    COPIED_SUCCESSFULLY:"COPY_EXISTING_TEMPLATE_COPIED_SUCCESSFULLY"
}

export const INCIDENT_CREATE = {
    VALID_INCIDENT_DATE:"IF_Incident_Create_Validate_Incident_Date",
    VALID_TRANSIT_DATE:"IF_Incident_Create_Validate_Transit_Date"
}

export const TEMPLATE_FORM_POPUP = {
    DUPLICATE_NAME:"IF_Template_Form_Popup_Duplicate_Validation",
    TEMPLATE_CREATED:"IF_Template_Form_Popup_Save"
}

export const PROPERTY_FORM_POPUP = {
    ENTER_OPTIONS : "Enter_Options",
    NO_DUPLICATE : "No_Duplicate",
    NO_BLANK : "No_Blank"
}

export const SERVICE_INTERRUPTION = {
    VALID_START_DATE:"IF_Service_Intrerruption_Validate_Start_Date",
    VALID_END_DATE:"IF_Service_Intrerruption_Validate_End_Date",
    VALID_RELEASE_DATE:"IF_Service_Intrerruption_Validate_Released_Date"
}

export const TRANSFER_REQUEST = {
    FIRST_CALL:"IF_Transfer_Request_Validate_First_Call",
    SECOND_CALL:"IF_Transfer_Request_Validate_Second_Call",
    ETA:"IF_Transfer_Request_Validate_ETA"
}

export const WHEEL_CHAIR = {
    SERVICE_TIME:"IF_Wheel_Chair_Validate_Service_Time",
    REPORTED_TIME:"IF_Wheel_Chair_Validate_Reported_Time",
    BOARDING_TIME:"IF_Wheel_Chair_Validate_Boarding_Time",
    DEBOARDING_TIME:"IF_Wheel_Chair_Validate_Deboarding_Time"
}
 