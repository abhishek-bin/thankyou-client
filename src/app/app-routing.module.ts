import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';

const routes: Routes = [
    {path: '', redirectTo: 'landing', pathMatch: 'full'},
    {path: 'landing', loadChildren: './landing/landing.module#LandingPageModule'},
    {path: 'sharing', loadChildren: './sharing/sharing.module#SharingModule'},
    {path: 'ui', loadChildren: './ui/ui.module#UIModule'}
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules, useHash: true})
    ],
    providers: [Diagnostic],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
