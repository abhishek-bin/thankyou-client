import {Component, OnInit} from '@angular/core';
import {NavController, Platform, Events} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Storage} from '@ionic/storage';
import {MenuController} from '@ionic/angular';
import {GlobalService} from './_core/services/global.service';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';
import {Facebook} from '@ionic-native/facebook/ngx';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {Router} from '@angular/router';
import {AppMinimize} from '@ionic-native/app-minimize/ngx';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
    constructor(
        private fb: Facebook,
        private socialSharing: SocialSharing,
        public fcm: FirebaseX,
        private globalService: GlobalService,
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private storage: Storage,
        private navCtrl: NavController,
        public menuCtrl: MenuController,
        private localNotifications: LocalNotifications,
        private gmail: GooglePlus,
        public route: Router, private appMinimize: AppMinimize,
    ) {
        this.initializeApp();

    }

    user: any;
    $: any;

    toggleMenu() {
        this.menuCtrl.toggle(); //Add this method to your button click function
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.platform.backButton.subscribeWithPriority(10, async () => {
                if (this.route.url === '/sharing/home' || this.route.url === '/landing/' || this.route.url.includes('sharing/message') ||
                    this.route.url === '/sharing/profile' || this.route.url === '/sharing/my-listing' || this.route.url === '/sharing/blog' ||
                    this.route.url === '/sharing/user-near-me' || this.route.url === '/sharing/need-help' || this.route.url === '/sharing/shop' ||
                    this.route.url === '/sharing/feedback' || this.route.url === '/sharing/invite' || this.route.url === '/sharing/terms') {
                    await this.appMinimize.minimize();
                } else if (this.route.url.includes('sharing/chat')) {
                    await this.navCtrl.navigateRoot('/sharing/message', {animated: true, animationDirection: 'back'});
                } else {
                    await this.navCtrl.back({animated: true, animationDirection: 'back'});
                    // await this.navCtrl.pop();
                }
            });
            this.splashScreen.hide();
            // this.statusBar.overlaysWebView(true);

            this.statusBar.styleLightContent();
            this.statusBar.show();
            this.fcm.onMessageReceived()
                .subscribe(async data => {
                    console.log(data);
                    if (data.tap) {
                        if (data.notificationType && data.notificationType === 'chat-message') {
                            setTimeout(async () => {
                                await this.navCtrl.navigateForward(`/chat/${data.senderId}`);
                            }, 500);

                        } else if (data.notificationType && data.notificationType === 'request-listing') {
                            setTimeout(async () => {
                                await this.navCtrl.navigateForward(`/chat/${data.senderId}`);
                            }, 500);

                        } else if (data.notificationType && data.notificationType === 'listing') {
                            console.log(`/sharing/detail/${data.productId}`);
                            setTimeout(async () => {
                                await this.navCtrl.navigateForward(`/sharing/detail/${data.productId}`);
                            }, 500);
                        }
                    } else {
                        this.localNotifications.on('click').subscribe((c) => {
                            console.log(c);
                            if (c.data.notificationType && c.data.notificationType === 'chat-message') {
                                setTimeout(async () => {
                                    await this.navCtrl.navigateForward(`/chat/${c.data.senderId}`);
                                }, 500);

                            } else if (c.data.notificationType && c.notificationType === 'request-listing') {
                                setTimeout(async () => {
                                    await this.navCtrl.navigateForward(`/chat/${c.data.senderId}`);
                                }, 500);

                            } else if (c.data.notificationType && c.data.notificationType === 'listing') {
                                console.log(`/sharing/detail/${c.data.productId}`);
                                setTimeout(async () => {
                                    await this.navCtrl.navigateForward(`/sharing/detail/${c.data.productId}`);
                                }, 500);
                            }
                        });
                        this.localNotifications.on('trigger').subscribe((c) => {
                            console.log(c);
                        });
                        this.localNotifications.schedule({
                            id: 1,
                            text: data.body,
                            title: data.title,
                            data,
                            vibrate: true,
                            foreground: true,

                        });

                    }
                });
        });
    }

    async logout() {
        if (this.user && this.user._id) {
            this.globalService.logout(this.user._id).subscribe(async (res) => {
                if (this.user.fbId) {
                    await this.fb.logout();
                } else if (this.user.gmailId) {
                    await this.gmail.logout();
                    await this.gmail.disconnect();
                }
            });
        }
        await this.storage.clear();
        await this.navCtrl.navigateRoot('/');
    }


    async ngOnInit() {
        console.log('called');
        this.user = await this.storage.get('user');
        this.globalService.currentMessage.subscribe((message) => {
            if (message) {
                console.log(message);
                this.user = message;
            }
        });
    }

    async getUserData() {
        this.user = await this.storage.get('user');
    }

    async navigateTo(url) {

        await this.navCtrl.navigateRoot([url], {replaceUrl: true});
    }

    async invite() {
        await this.socialSharing.shareWithOptions({
            url: 'https://play.google.com/store/apps/details?id=com.eeho.app',
            message: 'Hey! Join this amazing app to share and donate food/non-food'
        });
    }

}
