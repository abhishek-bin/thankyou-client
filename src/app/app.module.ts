import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {GlobalService} from './_core/services/global.service';
import {IonicStorageModule} from '@ionic/storage';
import {SharedModule} from './_core/modules/shared.modules';
import {LandingPageModule} from './landing/landing.module';
import {SharingModule} from './sharing/sharing.module';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {LoadingIconService} from './_core/services/loading-icon.service';
import {HTTP} from '@ionic-native/http/ngx';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import {SearchFilterComponent} from '../modal/search-filter/search-filter.component';
import {PayModalComponent} from './sharing/components/pay-modal/pay-modal.component';
import {NotificationComponent} from './sharing/components/notification/notification.component';
import {ReactiveFormsModule} from '@angular/forms';

import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import {StarRatingModule} from 'ionic5-star-rating';
import {HttpInterceptorService} from './_core/services/http-interceptor.service';
import {ErrorInterceptor} from './_core/services/error.interceptor';

@NgModule({
    declarations: [AppComponent, SearchFilterComponent, PayModalComponent, NotificationComponent],
    entryComponents: [SearchFilterComponent, PayModalComponent, NotificationComponent],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        IonicStorageModule.forRoot({
            name: '__mydb',
            driverOrder: ['indexeddb', 'sqlite', 'websql'],
        }),
        SharedModule,
        LandingPageModule,
        SharingModule,
        ReactiveFormsModule,
        StarRatingModule
    ],
    providers: [
        StatusBar,
        HTTP,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        GlobalService,
        LoadingIconService,
        HttpClient,
        LocalNotifications,
        GooglePlus,
        YoutubeVideoPlayer,
        AppMinimize,
        {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
// constructor(private statusBar: StatusBar) { }
// this.statusBar.overlaysWebView(true);
// this.statusBar.backgroundColorByHexString('#ffffff');

}
