import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {GlobalService} from '../_core/services/global.service';

@Injectable()

export class AnonymousGuard implements CanActivate {
    constructor(private authService: GlobalService, private router: Router) {
    }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // console.log(this.router.url)
        const token = await this.authService.getToken() || '';
        if (token.length !== 0 && this.authService.getToken()) {
            this.router.navigate(['/sharing/home'], {queryParams: {returnUrl: state.url}});
            return false;
        } else {
            return true;
        }
    }
}
