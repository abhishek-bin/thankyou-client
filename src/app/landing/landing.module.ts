import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {LandingPageComponent} from './landing.page';
import {RegisterComponent} from './register/register.component';
import {SharedModule} from '../_core/modules/shared.modules';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './login/login.component';
import {AnonymousGuard} from '../guards/anonymous.guard';
import {VerifyOtpComponent} from '../sharing/components/verify-otp/verify-otp.component';
import {CommonModule} from '@angular/common';
import {Facebook} from '@ionic-native/facebook/ngx';
import {ForgotPasswordComponent} from '../sharing/components/forgot-password/forgot-password.component';

@NgModule({
    declarations: [LandingPageComponent, VerifyOtpComponent, ForgotPasswordComponent],
    imports: [SharedModule,
        RouterModule.forChild([
            // {
            //     path: '',
            //     component: LandingPageComponent,
            //     canActivate: [AnonymousGuard]
            // },
            {
                path: '',
                component: LoginComponent,
                canActivate: [AnonymousGuard]
            },
            {
                path: 'register',
                component: RegisterComponent,
                canActivate: [AnonymousGuard]
            },
            {
                path: 'verify-otp',
                component: VerifyOtpComponent
            }, {
                path: 'forgot-password',
                component: ForgotPasswordComponent,
                canActivate: [AnonymousGuard]
            }
        ]),
        HttpClientModule,
        ReactiveFormsModule, CommonModule
    ],
    providers: [HttpClient, AnonymousGuard, Facebook
    ]
})
export class LandingPageModule {
}
