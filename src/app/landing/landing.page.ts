import {Component, ViewEncapsulation} from '@angular/core';
import {AlertController, NavController, ToastController} from '@ionic/angular';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook/ngx';
import {GlobalService} from '../_core/services/global.service';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {ResponseModel} from '../_core/data/api-response.model';
import {AppConstant} from '../_core/services/app.constant';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';
import {Storage} from '@ionic/storage';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';

@Component({
    selector: 'app-landing',
    templateUrl: 'landing.page.html',
    styleUrls: ['landing.page.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LandingPageComponent {

    constructor(private diagnostic: Diagnostic, private storage: Storage, public fcm: FirebaseX,
                private gmail: GooglePlus, private alertController: AlertController, private toastController: ToastController, private geolocation: Geolocation, private navCtrl: NavController, private fb: Facebook, private service: GlobalService) {
    }

    async routeToLogin() {
        await this.navCtrl.navigateForward('/landing/');
    }

    async routeToRegister() {
        await this.navCtrl.navigateForward('/landing/register');
    }

    async loginGmail() {
        this.gmail.login({}).then((resp) => {
            this.geolocation.getCurrentPosition().then(async (respGeo) => {
                await this.service.showLoader();
                const reqObj = {
                    location: [respGeo.coords.longitude, respGeo.coords.latitude],
                    name: resp.displayName,
                    email: resp.email,
                    gmailId: resp.userId,
                    profileUrl: resp.imageUrl
                };
                this.service.gmailLogin(reqObj).subscribe(async (res2: ResponseModel) => {
                    if (res2.status === AppConstant.CODE.SUCCESS) {
                        // this.submitted = false;
                        // this.registerForm.reset();
                        await this.service.closeLoader();
                        this.fcm.getToken().then((token) => {
                            this.service.saveDeviceToken({
                                userId: res2.responseData._id,
                                deviceToken: token
                            }).subscribe((r) => {
                            });
                        });
                        await this.storage.clear();
                        await this.storage.set('user', res2.responseData);
                        await this.storage.set('token', res2.responseData.token);
                        this.service.changeMessage(res2.responseData);

                        await this.navCtrl.navigateForward('/sharing/home');
                    }
                }, async (err) => {
                    await this.service.closeLoader();
                    console.log(err);
                    const alert = await this.alertController.create({
                        subHeader: 'Oops!',
                        message: err.message ? err.message : 'Internal server error',
                        buttons: ['OK'],
                        // cssClass: 'bg-red'
                    });
                    await alert.present();
                });
            });
        }).catch(async (er) => {
            console.log(er);
            await this.service.closeLoader();
        });
    }

    async loginFB() {
        const locEnabled = await this.diagnostic.isLocationEnabled();
        if (!locEnabled) {
            const toast = await this.toastController.create({
                message: 'Please enable location.',
                duration: 2000
            });
            await toast.present();
            return;
        }
        // await this.fb.logout();
        this.fb.login(['public_profile', 'email'])
            .then((res: FacebookLoginResponse) => {
                this.service.showLoader();
                console.log('Logged into Facebook!', res);
                const userId = res.authResponse.userID;

                this.fb.api('me' + '?fields=id,name,email,picture', ['public_profile']).then((resp) => {
                    console.log(resp);
                    this.geolocation.getCurrentPosition().then(async (respGeo) => {
                        const reqObj = {
                            location: [respGeo.coords.longitude, respGeo.coords.latitude],
                            name: resp.name,
                            email: resp.email,
                            fbId: resp.id,
                            profileUrl: `https://graph.facebook.com/${resp.id}/picture?type=large`
                            // profileUrl: resp.picture.data.url
                        };
                        await this.service.closeLoader();
                        this.service.fbLogin(reqObj).subscribe(async (res2: ResponseModel) => {
                            if (res2.status === AppConstant.CODE.SUCCESS) {
                                // this.submitted = false;
                                // this.registerForm.reset();
                                this.fcm.getToken().then((token) => {
                                    this.service.saveDeviceToken({userId: res2.responseData._id, deviceToken: token}).subscribe((r) => {
                                    });
                                });
                                await this.storage.clear();
                                await this.storage.set('user', res2.responseData);
                                await this.storage.set('token', res2.responseData.token);
                                this.service.changeMessage(res2.responseData);
                                await this.service.closeLoader();
                                await this.navCtrl.navigateForward('/sharing/home');
                            }
                        }, async (err) => {
                            await this.service.closeLoader();
                            console.log(err);
                            const alert = await this.alertController.create({
                                subHeader: 'Oops!',
                                message: err.message ? err.message : 'Internal server error',
                                buttons: ['OK'],
                                // cssClass: 'bg-red'
                            });
                            await alert.present();
                        });
                    }, async (err) => {
                        await this.service.closeLoader();
                        console.log(err);
                        const alert = await this.alertController.create({
                            subHeader: 'Oops!',
                            message: err.message ? err.message : 'Internal server error',
                            buttons: ['OK'],
                            // cssClass: 'bg-red'
                        });
                        await alert.present();
                    });
                });
            })

            .catch(async (e) => {
                await this.service.closeLoader();
                console.log('Error logging into Facebook', e);
            });


        this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);
    }
}
