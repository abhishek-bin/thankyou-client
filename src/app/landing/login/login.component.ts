import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AlertController, NavController, Events, ToastController} from '@ionic/angular';
import {LoadingIconService} from 'src/app/_core/services/loading-icon.service';
import {GlobalService} from '../../_core/services/global.service';
import {AppConstant} from '../../_core/services/app.constant';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';
import {Storage} from '@ionic/storage';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook/ngx';
import {ResponseModel} from '../../_core/data/api-response.model';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],

})
export class LoginComponent implements OnInit {
    private play = false;

    constructor(private fb: Facebook, public fcm: FirebaseX, public alertController: AlertController,
                private storage: Storage, private navCtrl: NavController, private gmail: GooglePlus,
                private loaderService: LoadingIconService, private formBuilder: FormBuilder,
                private globalService: GlobalService, private geolocation: Geolocation, private toastController: ToastController,
                private diagnostic: Diagnostic) {
    }

    email = '';
    password = '';
    phone = '';
    otp: '';
    loginForm: FormGroup;
    submitted = false;
    isOtpSent = false;
    time = 0;
    interval;

    ngOnInit() {

    }

    async navigateToForgot() {
        await this.navCtrl.navigateForward('/landing/forgot-password');

    }

    async routeToRegister() {
        await this.navCtrl.navigateForward('landing/register');
    }

    get f() {
        return this.loginForm.controls;
    }

    async routeToHome(flag) {

        const locEnabled = await this.diagnostic.isLocationEnabled();
        if (!locEnabled) {
            const toast = await this.toastController.create({
                message: 'Please enable location to continue!',
                duration: 2000
            });
            await toast.present();
            return;
        }
        if (flag === 1) {
            this.globalService.sendOtp('+91' + this.phone).subscribe(async (res) => {
                if (res.status === AppConstant.CODE.SUCCESS) {
                    const toast = await this.toastController.create({
                        message: 'OTP sent to your phone',
                        duration: 2000
                    });
                    await toast.present();
                    await this.globalService.closeLoader();
                    this.isOtpSent = true;
                    this.startTimer();
                } else {
                    await this.globalService.closeLoader();
                }
            }, async (err) => {
                await this.globalService.closeLoader();
                console.log(err);
                const alert = await this.alertController.create({
                    subHeader: 'Oops!',
                    message: err.message ? err.message : 'Internal server error',
                    buttons: ['OK'],
                    // cssClass: 'bg-red'
                });
                await alert.present();
            });
        } else if (flag === 2) {
            const req = {
                otp: this.otp,
                phone: '+91' + this.phone
            };
            this.globalService.verifyOtp(req).subscribe(async (res) => {
                if (res.status === AppConstant.CODE.SUCCESS) {
                    await this.globalService.closeLoader();
                    await this.storage.clear();
                    if (!Object.keys(res.responseData).length) {
                        await this.navCtrl.navigateForward(['/landing/register'], {queryParams: {phone: this.phone}, replaceUrl: true});
                    } else {
                        await this.storage.set('user', res.responseData);
                        await this.storage.set('token', res.responseData.token);
                        this.globalService.changeMessage(res.responseData);
                        this.fcm.getToken().then((token) => {
                            this.globalService.saveDeviceToken({userId: res.responseData._id, deviceToken: token}).subscribe((r) => {
                            });
                        });
                        await this.navCtrl.navigateForward(['/sharing/home'], {replaceUrl: true});
                    }
                } else {
                    await this.globalService.closeLoader();
                }
            }, async (err) => {
                await this.globalService.closeLoader();
                console.log(err);
                const alert = await this.alertController.create({
                    subHeader: 'Oops!',
                    message: err.message ? err.message : 'Internal server error',
                    buttons: ['OK'],
                    // cssClass: 'bg-red'
                });
                await alert.present();
            });
        }
    }


    startTimer() {
        // this.play = true;
        this.interval = setInterval(() => {
            this.time++;
            if (this.time === 30) {
                clearInterval(this.interval);
            }
        }, 1000);
    }

    pauseTimer() {
        // this.play = false;
        // clearInterval(this.interval);
    }

    async resendOtp() {
        this.time = 0;
        await this.routeToHome(1);
    }
}

