import {Component, OnInit} from '@angular/core';
import {UserModel} from '../../_core/data/user.model';
import {GlobalService} from '../../_core/services/global.service';
import {ResponseModel} from '../../_core/data/api-response.model';
import {AppConstant} from '../../_core/services/app.constant';
import {Storage} from '@ionic/storage';
import {AlertController, Events, NavController, ToastController} from '@ionic/angular';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook/ngx';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {ActivatedRoute} from '@angular/router';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

    user: UserModel;
    registerForm: FormGroup;
    submitted = false;
    tnc = false;
    phone = '';

    constructor(private gmail: GooglePlus, public fcm: FirebaseX, private fb: Facebook, private alertController: AlertController,
                private formBuilder: FormBuilder, public event: Events, private geolocation: Geolocation, private globalService: GlobalService,
                private storage: Storage, private navCtrl: NavController, private route: ActivatedRoute, private toastController: ToastController,
                private diagnostic: Diagnostic) {
    }

    get f() {
        return this.registerForm.controls;
    }

    async registerUser() {
        const locEnabled = await this.diagnostic.isLocationEnabled();
        if (!locEnabled) {
            const toast = await this.toastController.create({
                message: 'Please enable location to continue!',
                duration: 2000
            });
            await toast.present();
            return;
        }

        if (!this.tnc) {
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: 'Please agree to out terms and conditions',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        }
        this.user = new UserModel({});
        this.user.email = this.f.email.value;
        this.user.name = this.f.name.value;
        this.user.address = this.f.address.value;
        this.user.phone = '+91' + this.phone;

        this.geolocation.getCurrentPosition().then((resp) => {
            this.user.location = [resp.coords.longitude, resp.coords.latitude];
            this.globalService.register(this.user).subscribe(async (res: ResponseModel) => {
                if (res.status === AppConstant.CODE.SUCCESS) {
                    this.registerForm.reset();
                    await this.storage.set('user', res.responseData);
                    await this.storage.set('token', res.responseData.token);
                    this.globalService.changeMessage(res.responseData);
                    this.fcm.getToken().then((token) => {
                        this.globalService.saveDeviceToken({userId: res.responseData._id, deviceToken: token}).subscribe((r) => {
                        });
                    });
                    await this.navCtrl.navigateForward('/sharing/home');
                }
            }, async (err) => {
                await this.globalService.closeLoader();
                console.log(err);
                const alert = await this.alertController.create({
                    subHeader: 'Oops!',
                    message: err.message ? err.message : 'Internal server error',
                    buttons: ['OK'],
                    // cssClass: 'bg-red'
                });
                await alert.present();
            });
        });
    }

    tncChange(ev) {
        console.log(this.tnc);
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            name: ['', Validators.required],
            address: ['', Validators.required],
        });
    }

    async routeToLogin() {
        await this.navCtrl.navigateForward('/');
    }

    async ionViewWillEnter() {
        this.phone = this.route.snapshot.queryParams.phone;
        if (!this.phone) {
            await this.navCtrl.navigateRoot('/');
        }
    }
}
