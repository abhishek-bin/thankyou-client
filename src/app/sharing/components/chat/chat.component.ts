import {ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '../../../_core/services/global.service';
import {AppConstant} from '../../../_core/services/app.constant';
import {AlertController, Events, IonContent, NavController, ToastController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {FileTransfer, FileTransferObject, FileUploadOptions} from '@ionic-native/file-transfer/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {PhotoViewer} from '@ionic-native/photo-viewer/ngx';
import {File} from '@ionic-native/file/ngx';
import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, OnDestroy {

    senderId: any;
    receiverId: any;
    apiTimer = 0;
    @ViewChild('content') content: IonContent;
    @ViewChild('chat_input') messageInput: ElementRef;
    User: 'Me';
    inpText: any;
    user: any;
    receiverUser: any;
    msgList: Array<{
        senderId: any,
        receiverId: any,
        userName: any,
        type: any,
        userAvatar: any,
        time: any,
        message: any,
        upertext: any;
        addedOn: any;
        receiverProfileUrl: string;
        senderProfileUrl: string;
    }> = [];
    public count = 0;
    private fileTransfer: FileTransferObject;
    imageData: any;
    profileUrl = '';
    isBlocked = 0;

    constructor(private webView: WebView, private transfer: FileTransfer, private ref: ChangeDetectorRef, private camera: Camera, private navCtrl: NavController,
                // tslint:disable-next-line:max-line-length
                private route: ActivatedRoute, private toastController: ToastController, private alertController: AlertController, private storage: Storage,
                private fs: File, private events: Events, private activatedRoute: ActivatedRoute, private globalService: GlobalService, private photoViewer: PhotoViewer,
                private androidPermissions: AndroidPermissions) {


    }

    async ngOnInit() {
    }

    async ionViewWillEnter() {
        // await this.globalService.showLoader();
        const toast = await this.toastController.create({
            message: 'Loading chat.Please wait ',
            duration: 3000
        });
        await toast.present();
        this.receiverId = this.activatedRoute.snapshot.paramMap.get('receiverId');
        this.getPublicUser();
        this.markAllRead();

        this.user = await this.storage.get('user');
        this.senderId = this.user._id;
        this.getMessageList();
        setTimeout(async () => {
            await this.scrollToBottom();
        }, 100);
        this.apiTimer = setInterval(() => {
            const addedOn = this.msgList.length ? this.msgList[this.msgList.length - 1].addedOn : 0;
            this.globalService.getMessageList(this.senderId, this.receiverId, addedOn).subscribe(async (res) => {
                    if (res.status === AppConstant.CODE.SUCCESS) {
                        this.msgList = this.msgList.concat(res.responseData);
                        await this.scrollToBottom();
                    }
                    await this.globalService.closeLoader();

                }, async (err) => {
                    await this.globalService.closeLoader();

                    const toast1 = await this.toastController.create({
                        message: err.message,
                        duration: 2000
                    });
                    await toast1.present();
                }
            );
            // }

        }, 6000);
    }

    async scrollToBottom() {
        await this.content.scrollToBottom(10);
    }

    // ionViewWillLeave() {
    //     this.events.unsubscribe('chat:received');
    // }

    ionViewDidLeave() {
        this.events.unsubscribe('chat:received');
        clearInterval(this.apiTimer);
    }

    ngOnDestroy() {
        clearInterval(this.apiTimer);
    }

    sendMsg() {
        let reqObj = {};
        if (this.profileUrl.length) {
            const options: FileUploadOptions = {
                chunkedMode: false,
                fileName: this.imageData.substr(this.imageData.lastIndexOf('/') + 1)
            };
            this.fileTransfer.upload(this.imageData, AppConstant.BASE_URL + AppConstant.UPLOAD_MEDIA
                , options, true).then((res) => {
                console.log('file uploaded successfully.', res);
                const responseObj = JSON.parse(res.response);
                reqObj = {
                    senderId: this.user._id,
                    receiverId: this.receiverId,
                    message: responseObj.responseData.mediaUrl,
                    type: 'media'
                };
                this.globalService.sendMessage(reqObj).subscribe(async (resp) => {
                    if (resp.status === AppConstant.CODE.SUCCESS) {
                        this.clearUpload();
                        this.getMessageList();
                    }
                    console.log('scrollBottom');
                    await this.scrollToBottom();
                });
            });
        } else {
            reqObj = {
                senderId: this.user._id,
                receiverId: this.receiverId,
                message: this.inpText,
                type: 'text'
            };
            this.inpText = '';
            this.globalService.sendMessage(reqObj).subscribe(async (resp) => {
                if (resp.status === AppConstant.CODE.SUCCESS) {
                    this.msgList.push(this.inpText);

                    this.getMessageList();
                }
                console.log('scrollBottom');
                await this.scrollToBottom();
            });
        }
    }

    private getMessageList() {
        this.globalService.getMessageList(this.user._id, this.receiverId, 0).subscribe((res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                this.msgList = [...res.responseData];
            }
        });
    }

    private markAllRead() {
        console.log('this.receiverId', this.receiverId);
        this.globalService.markReadMessages(this.receiverId).subscribe((res) => {
            console.log(res);
        });
    }

    uploadOrTakePhoto() {
        const options: CameraOptions = {
            quality: 100,
            targetHeight: 650,
            targetWidth: 650,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
        this.camera.getPicture(options).then((imageData) => {
            this.imageData = imageData;
            const currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
            this.profileUrl = this.webView.convertFileSrc(imageData);
            // this.isUpdateClicked = true;
            this.ref.detectChanges();
            this.fileTransfer = this.transfer.create();

        }, (err) => {
            // Handle error
        });

    }

    clearUpload() {
        this.profileUrl = '';
        this.imageData = null;
    }

    async openImageViewer(url) {
        this.photoViewer.show(url, 'Preview', {
            share: true, // default is false
        });
        await this.downloadImage(url);
    }

    async downloadImage(url) {
        const fTransfer: FileTransferObject = this.transfer.create();
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.STORAGE).then(
            (result) => {
                console.log('Has permission?', result.hasPermission);
                // tslint:disable-next-line:max-line-length
                fTransfer.download(url, this.fs.dataDirectory + url.substring(url.lastIndexOf('/'), url.lastIndexOf('%'))).then(async (entry) => {
                    // console.log('download complete: ' + entry.toURL());
                    const toast = await this.toastController.create({
                        message: 'File saved in Downloads...',
                        duration: 3000
                    });
                    await toast.present();
                }, (error) => {
                    // handle error
                });
            },
            (err) => {
                this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.STORAGE);
                fTransfer.download(url, this.fs.dataDirectory + url.substring(url.lastIndexOf('/'), url.lastIndexOf('%'))).then(async (entry) => {
                    // console.log('download complete: ' + entry.toURL());
                    const toast = await this.toastController.create({
                        message: 'File saved in Downloads...',
                        duration: 3000
                    });
                    await toast.present();
                }, (error) => {
                    // handle error
                });
            }
        );
    }

    async showOptions() {
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: `Block ${this.receiverUser.name}?`,
            inputs: this.isBlocked && this.receiverUser.doneBy === this.user._id ? [{
                name: 'checkbox1',
                type: 'radio',
                label: 'UNBLOCK',
                value: 'unblock'
            }] : [
                {
                    name: 'checkbox1',
                    type: 'radio',
                    label: 'REPORT AND BLOCK',
                    value: 'report'
                },

                {
                    name: 'checkbox2',
                    type: 'radio',
                    label: 'BLOCK',
                    value: 'block'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: async (data) => {
                        if (data === 'block') {
                            this.globalService.blockUser(this.receiverId).subscribe((res: any) => {
                                if (res.status === AppConstant.CODE.SUCCESS) {
                                    this.getPublicUser();
                                }
                            });
                        } else if (data === 'report') {
                            this.globalService.blockUser(this.receiverId).subscribe((res: any) => {
                                if (res.status === AppConstant.CODE.SUCCESS) {
                                    this.globalService.reportUser(this.receiverId).subscribe((res1: any) => {
                                        if (res.status === AppConstant.CODE.SUCCESS) {
                                            this.isBlocked = 1;
                                            this.getPublicUser();
                                        }
                                    });
                                }
                            });
                        } else {
                            this.globalService.unBlockUser(this.receiverId).subscribe((res: any) => {
                                if (res.status === AppConstant.CODE.SUCCESS) {
                                    this.getPublicUser();
                                }
                            });
                        }

                    }
                }
            ]
        });

        await alert.present();
    }

    private getPublicUser() {
        this.globalService.getUserById(this.receiverId).subscribe((res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                this.receiverUser = res.responseData;
                this.isBlocked = res.responseData.isBlocked;
            }
        }, async () => {
            await this.globalService.closeLoader();
            const toast2 = await this.toastController.create({
                message: 'Something went wrong! Try again',
                duration: 3000
            });
            await toast2.present();
        });
    }
}




