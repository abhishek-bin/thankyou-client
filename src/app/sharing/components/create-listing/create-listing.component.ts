import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {FileTransfer, FileTransferObject, FileUploadOptions} from '@ionic-native/file-transfer/ngx';
import {AlertController, Events, NavController} from '@ionic/angular';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {GlobalService} from '../../../_core/services/global.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Storage} from '@ionic/storage';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {AppConstant} from '../../../_core/services/app.constant';

import * as moment from 'moment';
import {TabSelectionsService} from '../../services/tab-selections.service';

@Component({
    selector: 'app-create-listing',
    templateUrl: './create-listing.component.html',
    styleUrls: ['./create-listing.component.scss'],
    changeDetection: ChangeDetectionStrategy.Default
})
export class CreateListingComponent implements OnInit {
    private fileTransfer: FileTransferObject;
    profileUrl: string;
    imageData: any;
    donateSubmitted = false;
    wantSubmitted = false;
    isDonated = true;
    type = true;
    wantForm: FormGroup;
    donateForm: FormGroup;
    user: any;
    currentYear = (new Date()).getFullYear() + 5 + '-12-31';
    minYear = (new Date()).getFullYear() + '-' + (((new Date()).getMonth() + 1) > 10 ? ((new Date()).getMonth() + 1) : '0' + ((new Date()).getMonth() + 1)) + '-' +
        (((new Date()).getDate()) > 10 ? (new Date()).getDate() : '0' + ((new Date()).getDate()));

    constructor(private geolocation: Geolocation, private storage: Storage, private navCtrl: NavController, private tabService: TabSelectionsService,
                private alertController: AlertController, private formBuilder: FormBuilder,
                private transfer: FileTransfer, public event: Events, private webView: WebView,
                private ref: ChangeDetectorRef, private camera: Camera, private appService: GlobalService) {
    }

    updateToggle() {
        this.isDonated = !this.isDonated;
        this.setForm();

    }

    foodType() {
        this.type = !this.type;
    }

    get df() {
        return this.donateForm.controls;
    }

    get wf() {
        return this.wantForm.controls;
    }

    ngOnInit() {
        this.setForm();

    }


    uploadOrTakePhoto() {
        const options: CameraOptions = {
            quality: 100,
            targetHeight: 650,
            targetWidth: 650,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
        this.camera.getPicture(options).then((imageData) => {
            this.imageData = imageData;
            const currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
            this.profileUrl = this.webView.convertFileSrc(imageData);
            // this.isUpdateClicked = true;
            this.ref.detectChanges();
            this.fileTransfer = this.transfer.create();

        }, (err) => {
            // Handle error
        });

    }

    async submitDonate() {
        if (this.type) {
            this.df.expiryTime.setValidators(Validators.required);
        } else {
            this.df.expiryTime.clearValidators();
            this.df.expiryTime.setErrors(null);
        }
        // if (this.type) {
        //     this.donateForm.get('expiryTime').setValidators(Validators.required);
        // } else {
        //     this.donateForm.get('expiryTime').clearValidators();
        //     this.donateForm.get('expiryTime').setErrors(null);
        // }

        this.user = await this.storage.get('user');
        this.donateSubmitted = true;
        if (this.donateForm.invalid) {
            return;
        }
        await this.appService.showLoader();

        const reqObj = {
            title: this.df.title.value,
            description: this.df.description.value,
            addedBy: this.user._id,
            category: this.df.category.value,
            type: this.type ? 'food' : 'non-food',
            location: [],
            mediaUrl: '',
            from: '',
            to: ''
        };

        if (this.df.from.value) {
            reqObj.from = moment((this.df.from.value)).format('hh:mm a').toUpperCase();
        }
        if (this.df.to.value) {
            reqObj.to = moment((this.df.to.value)).format('hh:mm a').toUpperCase();
        }
        if (reqObj.type === 'food') {
            // @ts-ignore
            reqObj.expiryTime = new Date(this.df.expiryTime.value).getTime();
        }

        const options: FileUploadOptions = {
            chunkedMode: false,
            fileName: this.imageData.substr(this.imageData.lastIndexOf('/') + 1)
        };
        this.geolocation.getCurrentPosition().then((resp) => {
            reqObj.location = [resp.coords.longitude, resp.coords.latitude];
            this.fileTransfer.upload(this.imageData, AppConstant.BASE_URL + AppConstant.UPLOAD_MEDIA
                , options, true).then((res) => {
                console.log('file uploaded successfully.', res);
                const responseObj = JSON.parse(res.response);
                reqObj.mediaUrl = responseObj.responseData.mediaUrl;
                this.appService.donate(reqObj).subscribe(async (respo) => {
                    await this.appService.closeLoader();
                    this.profileUrl = '';
                    this.donateSubmitted = false;
                    this.setForm();
                    this.df.from.setValue(0);
                    this.df.expiryTime.setValue(null);
                    this.df.to.setValue(0);
                    const alert = await this.alertController.create({
                        subHeader: '',
                        message: respo.message,
                        buttons: ['OK'],
                        // cssClass: 'bg-red'
                    });
                    await alert.present();
                    this.df.expiryTime.setValue(null);
                }, async (err) => {
                    await this.appService.closeLoader();
                    this.setForm();
                    this.donateSubmitted = false;
                    console.log(err);
                    const alert = await this.alertController.create({
                        subHeader: 'Oops!',
                        message: err.message ? err.message : 'Internal server error',
                        buttons: ['OK'],
                        // cssClass: 'bg-red'
                    });

                    await alert.present();
                });
            });
        });
    }

    async submitWant() {
        this.user = await this.storage.get('user');
        if (this.type) {
            this.wf.expiryTime.setValidators(Validators.required);
        } else {
            this.wf.expiryTime.clearValidators();
            this.wf.expiryTime.setErrors(null);
        }
        this.wantSubmitted = true;
        if (this.wantForm.invalid) {
            return;
        }
        await this.appService.showLoader();
        const reqObj = {
            title: this.wf.title1.value,
            description: this.wf.description1.value,
            addedBy: this.user._id,
            type: this.type ? 'food' : 'non-food',
            location: [],
            category: this.wf.category.value,

        };
        if (reqObj.type === 'food') {

            // @ts-ignore
            reqObj.expiryTime = new Date(this.wf.expiryTime.value).getTime();
        }
        this.geolocation.getCurrentPosition().then((resp) => {
            reqObj.location = [resp.coords.longitude, resp.coords.latitude];
            this.appService.want(reqObj).subscribe(async (res) => {
                this.wantSubmitted = false;
                this.setForm();
                this.profileUrl = '';
                this.wf.expiryTime.setValue(null);
                await this.appService.closeLoader();
                const alert = await this.alertController.create({
                    subHeader: '',
                    message: res.message,
                    buttons: ['OK'],
                    // cssClass: 'bg-red'
                });
                await alert.present();
            }, async (err) => {
                await this.appService.closeLoader();
                console.log(err);
                this.setForm();
                this.wantSubmitted = false;
                const alert = await this.alertController.create({
                    subHeader: 'Oops!',
                    message: err.message ? err.message : 'Internal server error',
                    buttons: ['OK'],
                    // cssClass: 'bg-red'
                });
                await alert.present();
            });
        });
    }


    async onSwipeRight($event: any) {
        await this.navCtrl.navigateForward(`/sharing/health`, {replaceUrl: true});
        this.tabService.updateTabSelectedBar('health', 3);
    }

    async onSwipeLeft($event: any) {
        await this.navCtrl.navigateBack(`/sharing/home`, {replaceUrl: true});
        this.tabService.updateTabSelected('non-food');
        this.tabService.updateTabSelectedBar('home', 1);
    }

    private setForm() {
        this.donateForm = this.formBuilder.group({
            title: ['', Validators.required],
            description: ['', Validators.required],
            from: [0,],
            to: [0,],
            expiryTime: [null, Validators.required],
            category: [null, Validators.required]
        });
        this.wantForm = this.formBuilder.group({
            title1: ['', Validators.required],
            description1: ['', Validators.required],
            expiryTime: [, Validators.required],
            category: [null, Validators.required],
        });
    }
}
