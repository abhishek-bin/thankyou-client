import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Storage} from '@ionic/storage';
import {GlobalService} from '../../../_core/services/global.service';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-feedback',
    templateUrl: './feedback.component.html',
    styleUrls: ['./feedback.component.scss'],
})
export class FeedbackComponent implements OnInit {

    helpSubmitted = false;
    helpForm: FormGroup;
    user: any;

    constructor(private storage: Storage, private formBuilder: FormBuilder, private appService: GlobalService,
                private alertController: AlertController) {
    }

    get df() {
        return this.helpForm.controls;
    }

    ngOnInit() {
        this.helpForm = this.formBuilder.group({
            description: ['', Validators.required]
        });
    }

    async submitHelp() {
        this.user = await this.storage.get('user');
        this.helpSubmitted = true;
        if (this.helpForm.invalid) {
            return;
        }
        await this.appService.showLoader();

        const reqObj = {
            feedback: this.df.description.value,
            userId: this.user._id
        };

        this.appService.feedback(reqObj).subscribe(async (respo) => {
            await this.appService.closeLoader();
            this.helpSubmitted = false;
            this.helpForm.reset();
            const alert = await this.alertController.create({
                subHeader: 'Success!',
                message: respo.message,
                buttons: ['OK'],
            });
            await alert.present();
        }, async (err) => {
            await this.appService.closeLoader();
            this.helpForm.reset();
            this.helpSubmitted = false;
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });

            await alert.present();
        });
    }
}
