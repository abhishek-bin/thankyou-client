import {Component, OnInit} from '@angular/core';
import {AlertController, NavController, ToastController} from '@ionic/angular';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';
import {Storage} from '@ionic/storage';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '../../../_core/services/global.service';
import {AppConstant} from '../../../_core/services/app.constant';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {

    phone = '';
    password = '';
    otp = '';
    isOtpSent = 0;

    constructor(public toastController: ToastController, public fcm: FirebaseX, private storage: Storage, private alertController: AlertController, private router: ActivatedRoute, private nav: NavController, private service: GlobalService
    ) {
    }

    ngOnInit() {
    }

    async sendOtp() {
        if (!this.phone.length) {
            const toast = await this.toastController.create({
                message: 'Phone cannot be empty!',
                duration: 3000
            });
            await toast.present();
            return;
        }
        this.isOtpSent = 1;
        this.service.resetOtp('+91' + this.phone).subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                const toast = await this.toastController.create({
                    message: 'OTP sent successfully',
                    duration: 2000
                });
                await toast.present();
                await this.service.closeLoader();
            } else {
                await this.service.closeLoader();
            }
        }, async (err) => {
            await this.service.closeLoader();
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }

    async resetPassword() {
        if (!this.otp) {
            const toast = await this.toastController.create({
                message: 'Otp cannot be empty!',
                duration: 3000
            });
            await toast.present();
            return;
        } else if (!this.password.length) {
            const toast = await this.toastController.create({
                message: 'Password cannot be empty!',
                duration: 3000
            });
            await toast.present();
            return;
        }
        const req = {
            otp: this.otp,
            phone: '+91' + this.phone,
            password: this.password
        };
        this.service.resetPassword(req).subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                await this.service.closeLoader();
                const toast = await this.toastController.create({
                    message: res.message,
                    duration: 3000
                });
                await toast.present();
                await this.nav.navigateForward('/landing/');
            } else {
                await this.service.closeLoader();
            }
        }, async (err) => {
            await this.service.closeLoader();
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }


}
