import {Component, OnInit} from '@angular/core';
import {GlobalService} from '../../../_core/services/global.service';
import {AlertController} from '@ionic/angular';
import {AppConstant} from '../../../_core/services/app.constant';
import * as moment from 'moment';
import {ActivatedRoute} from '@angular/router';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-news-blog-detail',
    templateUrl: './forum-detail.component.html',
    styleUrls: ['./forum-detail.component.scss'],
})
export class ForumDetailComponent implements OnInit {

    doShowComment = false;
    blog = {
        title: '',
        addedOn: 0,
        description: '',
        comment: '',
        like: [],
        profileUrl: ''
    };
    blogId = '';
    moment: any;
    user: any;
    inpText: '';

    constructor(private storage: Storage, private activatedRoute: ActivatedRoute, private globalService: GlobalService, private alertController: AlertController) {
        this.moment = moment;
    }

    async ngOnInit() {
        this.user = await this.storage.get('user');
        this.blogId = this.activatedRoute.snapshot.paramMap.get('forumId');
        await this.getBlog();
    }

    async getBlog() {
        await this.globalService.showLoader();
        this.globalService.getBlog(this.blogId).subscribe(async (res) => {
            await this.globalService.closeLoader();
            if (res.status === AppConstant.CODE.SUCCESS) {
                this.blog = res.responseData;
            }
        }, async (err) => {
            await this.globalService.closeLoader();
            const alert = await this.alertController.create({
                subHeader: 'Oops',
                message: err.message,
                buttons: ['OK']
            });
            await alert.present();
        });
    }

    likeBlog() {
        this.globalService.likeBlog(this.blogId).subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                await this.globalService.closeLoader();
                await this.getBlog();
            } else {
                await this.globalService.closeLoader();
            }
        }, async (err) => {
            await this.globalService.closeLoader();
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }

    async comment() {
        if (!this.inpText) {
            return;
        }
        const reqObj = {
            userId: this.user._id,
            blogId: this.blogId,
            text: this.inpText

        };
        await this.globalService.showLoader();
        this.globalService.commentBlog(reqObj).subscribe(async (res) => {
            this.inpText = '';
            if (res.status === AppConstant.CODE.SUCCESS) {
                await this.globalService.closeLoader();
                await this.getBlog();
            } else {
                await this.globalService.closeLoader();
            }
        }, async (err) => {
            this.inpText = '';
            await this.globalService.closeLoader();
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }

    showComment() {
        this.doShowComment ? this.doShowComment = false : this.doShowComment = true;
    }
}
