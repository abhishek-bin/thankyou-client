import {Component, OnInit} from '@angular/core';
import {GlobalService} from '../../../_core/services/global.service';
import {AppConstant} from '../../../_core/services/app.constant';
import {AlertController, NavController} from '@ionic/angular';
import * as moment from 'moment';

@Component({
    selector: 'app-news-blog-list',
    templateUrl: './forum.component.html',
    styleUrls: ['./forum.component.scss'],
})
export class ForumComponent implements OnInit {

    blogList = [];
    moment: any;

    constructor(private navCtrl: NavController, private globalService: GlobalService, private alertController: AlertController) {
        this.moment = moment;
    }

    async ngOnInit() {

    }
    async ionViewWillEnter() {
        await this.getBlogList();

    }
    async getBlogList() {

        await this.globalService.showLoader();
        this.globalService.getBlog().subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                this.blogList = res.responseData;
                await this.globalService.closeLoader();
            }
        }, async (err) => {
            await this.globalService.closeLoader();
            const alert = await this.alertController.create({
                header: 'Alert',
                subHeader: 'Subtitle',
                message: err.message,
                buttons: ['OK']
            });
            await alert.present();
        });
    }

    async navigate(id) {
        await this.navCtrl.navigateForward(`/forum/${id}`);
    }
}
