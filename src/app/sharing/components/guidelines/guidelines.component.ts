import {Component, OnInit} from '@angular/core';
import {Events, ModalController} from '@ionic/angular';
import {GlobalService} from '../../../../app/_core/services/global.service';

@Component({
    selector: 'app-guidelines',
    templateUrl: './guidelines.component.html',
    styleUrls: ['./guidelines.component.scss'],
})
export class GuidelinesComponent implements OnInit {
    type = '';
    distance = '';
    wanted = '';

    constructor(private globalService: GlobalService, private event: Events, private modalCtrl: ModalController) {
    }

    ngOnInit() {
        const filter = this.globalService.getCurrentFilter();
        if (filter && filter.hasOwnProperty('type')) {
            // @ts-ignore
            this.type = filter.type;
        }
    }

    closeModal() {
        this.modalCtrl.dismiss();
    }

    modalshow() {
        document.querySelector('.custom-modal').classList.add('show');
        document.querySelector('.donation-history-ui').classList.add('d-none');
    }
}
