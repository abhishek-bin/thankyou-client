import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealtheduComponent } from './healthedu.component';

describe('HealtheduComponent', () => {
  let component: HealtheduComponent;
  let fixture: ComponentFixture<HealtheduComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealtheduComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealtheduComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
