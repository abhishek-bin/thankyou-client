import {Component, OnInit} from '@angular/core';
import {GlobalService} from '../../../_core/services/global.service';
import {AlertController, Events, ModalController, NavController, ToastController} from '@ionic/angular';
import {SearchFilterComponent} from '../../../../modal/search-filter/search-filter.component';
import {PayModalComponent} from '../pay-modal/pay-modal.component';
import {Storage} from '@ionic/storage';
import {AppConstant} from '../../../_core/services/app.constant';
import {CameraOptions} from '@ionic-native/camera/ngx';
import {TabSelectionsService} from '../../services/tab-selections.service';

@Component({
    selector: 'app-healthedu',
    templateUrl: './healthedu.component.html',
    styleUrls: ['./healthedu.component.scss'],
})
export class HealtheduComponent implements OnInit {

    type = '';
    distance = '';
    wanted = '';
    user: any;
    modal: any;
    donationType = '';
    isResetVisible = false;
    donationList = [];
    donationListCopy = [];

    constructor(private alertCtrl: AlertController, private tabService: TabSelectionsService, protected toastController: ToastController, private navCtrl: NavController, private globalService: GlobalService, private event: Events, private modalCtrl: ModalController, private storage: Storage) {
    }

    async ngOnInit() {
        // this.user = await this.storage.get('user');
        // await this.getDonationList();
    }

    async ionViewWillEnter() {
        this.user = await this.storage.get('user');
        const prompt = await this.alertCtrl.create({
            header: 'Select',
            inputs: [
                {
                    value: 'health',
                    label: 'Health Support',
                    type: 'radio'
                }, {
                    value: 'education',
                    label: 'Education Support',
                    type: 'radio'
                },
                {
                    value: 'talent',
                    label: 'Talent Support',
                    type: 'radio'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: async () => {
                        await this.getDonationList();
                    }
                },
                {
                    text: 'OK',
                    handler: async data => {
                        this.donationType = data;
                        console.log(this.donationType);
                        await this.getDonationList();

                    }
                }
            ]
        });
        await prompt.present();


    }


    async modalShow() {
        this.modal = await this.modalCtrl.create({
            component: PayModalComponent
        });
        this.modal.onDidDismiss()
            .then((data) => {
                this.getDonationList();
            });

        return await this.modal.present();
    }

    async navigateToPay(donation) {
        if (donation.raisedAmount === donation.targetAmount) {
            const toast = await this.toastController.create({
                message: 'Donation Limit Reached!',
                duration: 3000
            });
            await toast.present();
            return;
        }
        await this.navCtrl.navigateForward(['/sharing/pay-donation'], {
            queryParams:
                {
                    donationId: donation._id
                },
        });
    }

    updateFilter() {
        if (!this.donationType || !this.donationType.length) {
            return;
        }
        this.donationList = [];
        this.donationList = this.donationListCopy.filter(don => don.type === this.donationType);
        this.isResetVisible = true;
    }

    reset() {
        this.isResetVisible = false;
        this.donationType = '';
        this.donationList = [...this.donationListCopy];

    }

    async getDonationList() {
        await this.globalService.showLoader();
        this.globalService.getDonation().subscribe(async (response) => {
            if (response.status === AppConstant.CODE.SUCCESS) {
                this.donationList = response.responseData;
                this.donationListCopy = response.responseData;
                this.updateFilter();
                await this.globalService.closeLoader();
                const toast = await this.toastController.create({
                    message: response.message,
                    duration: 3000
                });
                // await toast.present();
            }
        }, async (err) => {
            await this.globalService.closeLoader();
            const toast = await this.toastController.create({
                message: err.message,
                duration: 3000
            });
            await toast.present();
        });
    }

    async onSwipeRight($event: any) {
        await this.navCtrl.navigateForward(`/sharing/message`, {replaceUrl: true});
        this.tabService.updateTabSelectedBar('message', 4);
    }

    async onSwipeLeft($event: any) {
        await this.navCtrl.navigateBack(`/sharing/createListing`, {replaceUrl: true});
        this.tabService.updateTabSelectedBar('createListing', 2);
    }
}
