import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {LoadingIconService} from 'src/app/_core/services/loading-icon.service';
import {TabSelectionsService} from '../../services/tab-selections.service';
import {GlobalService} from '../../../_core/services/global.service';
import {ProductModel} from '../../../_core/data/product.model';
import {AppConstant} from '../../../_core/services/app.constant';
import * as moment from 'moment';
import {AlertController, Events, ModalController, NavController, Platform, ToastController} from '@ionic/angular';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {Storage} from '@ionic/storage';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';
import {SearchFilterComponent} from '../../../../modal/search-filter/search-filter.component';
import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
    tabSelected = 'food';
    user: any;
    productList: ProductModel[] = [];
    tempProductList: ProductModel[];
    productTypeList = [];
    filter = {};
    message = '';
    viewType = 'grid';


    constructor(private platform: Platform, private firebase: FirebaseX, private alertController: AlertController,
                private storage: Storage, public geolocation: Geolocation, public event: Events,
                private modalCtrl: ModalController, private loaderService: LoadingIconService,
                private tabService: TabSelectionsService, private globalService: GlobalService, private navCtrl: NavController,
                private androidPermissions: AndroidPermissions, private toastController: ToastController,
                private diagnostic: Diagnostic) {
    }

    async ionViewWillEnter() {
        this.user = await this.storage.get('user');
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
            (result) => {
                console.log(result);
            },
            (err) => {
                console.log(err);
                this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION);
            }
        );
        const locEnabled = await this.diagnostic.isLocationEnabled();
        if (!locEnabled) {
            const toast = await this.toastController.create({
                message: 'Please enable location to continue!',
                duration: 3000
            });
            await toast.present();
            return;
        }
        await this.getProductList();
    }

    updateViewType(type) {
        this.viewType = type;
    }

    async searchFilter() {
        const modal = await this.modalCtrl.create({
            component: SearchFilterComponent
        });
        return await modal.present();
    }

    async searchItem(ev) {

        this.message = ev.target.value;
        if (this.message.length) {
            this.productList = this.productList.filter(o => o.title.toLowerCase().includes(this.message.toLowerCase()) || o.description.toLowerCase().includes(this.message.toLowerCase()));
        } else {
            this.productList = [...this.tempProductList];
        }
    }

    async ngOnInit() {

        this.event.subscribe('filter', (filter) => {
            // user and time are the same arguments passed in `events.publish(user, time)`
            this.filter = filter;
            this.productList = [...this.tempProductList];
            if (filter.hasOwnProperty('type')) {
                this.productList = this.productList.filter(o => o.type === filter.type);
                this.productTypeList = this.productList.map(o => o.type);

            }
            if (filter.hasOwnProperty('wanted') && +filter.wanted === 0) {
                this.productList = this.productList.filter(o => o.isDonated !== +filter.wanted);
                this.productTypeList = this.productList.map(o => o.type);

            }
            if (filter.hasOwnProperty('distance')) {
                this.productList = this.productList.filter(o => o.distance <= (filter.distance * 1000));
                this.productTypeList = this.productList.map(o => o.type);
            }
            if (filter.hasOwnProperty('category')) {
                this.productList = this.productList.filter(o => filter.category.includes(o.category));
                this.productTypeList = this.productList.map(o => o.type);
            }

            console.log(this.productList);
            this.globalService.setCurrentFilter(this.filter);
        });
        this.event.subscribe('reset-filter', (filter) => {
            // user and time are the same arguments passed in `events.publish(user, time)`
            this.productList = this.tempProductList.filter(o => o);
            this.productTypeList = this.productList.map(o => o.type);

        });
        this.tabService.$tabSelected.subscribe(res => {
            this.tabSelected = res;
            if (!this.tabSelected) {
                this.tabSelected = 'food';
            }
        });
        // this.globalService.currentSearchMessage.subscribe((message) => {
        //     console.log('message', message );
        //     if (message) {
        //         this.productList = this.productList.filter(o => o.name.includes(message) || o.description.includes(message));
        //     } else {
        //         this.productList = [...this.tempProductList];
        //     }
        // });
    }

    async getProductList() {
        await this.globalService.showLoader();
        this.geolocation.getCurrentPosition().then((resp) => {
            console.log(resp.coords.longitude);
            this.globalService.getProductByParam(this.user._id, resp).subscribe(async (res) => {
                if (res.status === AppConstant.CODE.SUCCESS) {
                    await this.globalService.closeLoader();
                    this.productList = res.responseData.map((obj) => {
                        obj.addedOn = moment(obj.addedOn).fromNow();
                        return obj;
                    });
                    this.productTypeList = this.productList.map(o => o.type);
                    this.tempProductList = [...this.productList];
                    console.log(this.productList);
                } else {
                    await this.globalService.closeLoader();
                }
            }, async (err) => {
                await this.globalService.closeLoader();
                console.log(err);
                const alert = await this.alertController.create({
                    subHeader: 'Oops!',
                    message: 'Internal server error',
                    buttons: ['OK'],
                    // cssClass: 'bg-red'
                });
                await alert.present();
            });
        }, async (err) => {
            await this.globalService.closeLoader();
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        }).catch(async () => {
            await this.globalService.closeLoader();
        });
    }

    async getProductDetail(productId) {
        await this.navCtrl.navigateForward(`/sharing/detail/${productId}`);

    }

    async onSwipeRight($event: any) {
        if (this.tabSelected === 'food') {
            await this.navCtrl.navigateForward(`/sharing/home`, {replaceUrl: true});
            this.tabService.updateTabSelected('non-food');
            this.tabService.updateTabSelectedBar('home', 1);
        } else {
            await this.navCtrl.navigateForward(`/sharing/createListing`, {replaceUrl: true});
            // this.tabService.updateTabSelected('non-food');
            this.tabService.updateTabSelectedBar('createListing', 2);
        }

    }

    async onSwipeLeft($event: any) {
        if (this.tabSelected === 'non-food') {
            await this.navCtrl.navigateForward(`/sharing/home`, {replaceUrl: true});
            this.tabService.updateTabSelected('food');
            this.tabService.updateTabSelectedBar('home', 0);
        }

    }
}
