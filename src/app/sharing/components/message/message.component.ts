import {Component, OnInit} from '@angular/core';
import {AppConstant} from '../../../_core/services/app.constant';
import {GlobalService} from '../../../_core/services/global.service';
import {AlertController, NavController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {NavigationExtras} from '@angular/router';
import {TabSelectionsService} from '../../services/tab-selections.service';

@Component({
    selector: 'app-message',
    templateUrl: './message.component.html',
    styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {

    userList = [];
    user: any;
    showCheckbox = false;
    chatIdList = [];
    userRequestList = [];

    constructor(private tabService: TabSelectionsService, private navCtrl: NavController, private globalService: GlobalService, private alertController: AlertController, private storage: Storage) {
    }

    async ngOnInit() {
        //     this.user = await this.storage.get('user');
        //     await this.getChatUserList();
    }


    async getChatUserList() {
        await this.globalService.showLoader();
        this.globalService.getChatUserList(this.user._id).subscribe(async (res) => {
            await this.globalService.closeLoader();
            if (res.status === AppConstant.CODE.SUCCESS) {
                this.userList = res.responseData.chatHistory;
                this.userRequestList = res.responseData.chatRequestList;
            }
        }, async (err) => {
            await this.globalService.closeLoader();
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }

    async showDelete() {
        this.showCheckbox = true;
    }

    async hideDelete() {
        this.showCheckbox = false;
    }

    getChatId(id, value) {
        if (value && this.chatIdList.indexOf(id) === -1) {
            this.chatIdList.push(id);
        } else {
            const index = this.chatIdList.indexOf(id);
            index > -1 ? this.chatIdList.splice(index, 1) : '';
        }
        console.log(this.chatIdList);
    }

    deleteChat() {
        this.globalService.deleteChat({receiverList: this.chatIdList}).subscribe(async (res) => {
            this.showCheckbox = false;
            await this.getChatUserList();
        }, async (err) => {
            this.showCheckbox = false;
            await this.globalService.closeLoader();
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }

    async navigateToChat(user) {
        // const navigationExtras: NavigationExtras = {
        //     state: {
        //         user
        //     }
        // };
        await this.navCtrl.navigateForward([`/sharing/chat/${user._id}`], {replaceUrl: true});
        // await this.navCtrl.navigateForward([`/chat/${user._id}`], navigationExtras);
    }

    async ionViewWillEnter() {
        this.user = await this.storage.get('user');
        await this.getChatUserList();
    }

    async onSwipeLeft($event: any) {
        await this.navCtrl.navigateBack(`/sharing/health`, {replaceUrl: true});
        this.tabService.updateTabSelectedBar('health', 3);
    }

    acceptRequest(id) {
        this.globalService.acceptRequest(id).subscribe(async (res) => {
            this.showCheckbox = false;
            await this.getChatUserList();
        }, async (err) => {
            this.showCheckbox = false;
            await this.globalService.closeLoader();
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }

    rejectRequest(id) {
        this.globalService.rejectRequest(id).subscribe(async (res) => {
            this.showCheckbox = false;
            await this.getChatUserList();
        }, async (err) => {
            this.showCheckbox = false;
            await this.globalService.closeLoader();
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }
}

