import {Component, OnInit} from '@angular/core';
import {ProductModel} from '../../../_core/data/product.model';
import {AlertController, Events, ModalController, NavController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {LoadingIconService} from '../../../_core/services/loading-icon.service';
import {TabSelectionsService} from '../../services/tab-selections.service';
import {GlobalService} from '../../../_core/services/global.service';
import {AppConstant} from '../../../_core/services/app.constant';
import * as moment from 'moment';

@Component({
    selector: 'app-my-listing',
    templateUrl: './my-listing.component.html',
    styleUrls: ['./my-listing.component.scss'],
})
export class MyListingComponent implements OnInit {

    tabSelected = 'food';
    user: any;
    productList: ProductModel[] = [];
    tempProductList: ProductModel[];
    filter = {};

    constructor(private alertController: AlertController, private storage: Storage,
                public geolocation: Geolocation, public event: Events, private modalCtrl: ModalController,
                private loaderService: LoadingIconService, private tabService: TabSelectionsService,
                private globalService: GlobalService, private navCtrl: NavController) {
    }

    async ngOnInit() {
        // this.loaderService.LoadingOff();
        // this.user = await this.storage.get('user');
        // await this.getProductList();
    }

    async getProductList() {
        await this.globalService.showLoader();
        this.globalService.getMyProduct(this.user._id).subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                await this.globalService.closeLoader();
                this.productList = res.responseData.map((obj) => {
                    obj.addedOn = moment(obj.addedOn).fromNow();
                    return obj;
                });
                this.tempProductList = [...this.productList];
                console.log(this.productList);
            } else {
                await this.globalService.closeLoader();
            }
        }, async (err) => {
            await this.globalService.closeLoader();
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }

    async getProductDetail(productId) {
        await this.navCtrl.navigateForward(`/sharing/detail/${productId}`);
    }

    async ionViewWillEnter() {
        this.user = await this.storage.get('user');
        await this.getProductList();
    }

    async markAsDone(id) {
        const alert = await this.alertController.create({
            header: 'Confirm',
            message: 'Are you sure?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Okay',
                    handler: async () => {
                        await this.globalService.showLoader();
                        this.globalService.updateProduct(id + '?status=done').subscribe(async (res) => {
                            if (res.status === AppConstant.CODE.SUCCESS) {
                                await this.globalService.closeLoader();
                                await this.getProductList();
                            } else {
                                await this.globalService.closeLoader();
                            }
                        }, async (err) => {
                            await this.globalService.closeLoader();
                            console.log(err);
                            const aler = await this.alertController.create({
                                subHeader: 'Oops!',
                                message: err.message ? err.message : 'Internal server error',
                                buttons: ['OK'],
                                // cssClass: 'bg-red'
                            });
                            await aler.present();
                        });
                    }
                }
            ]
        });

        await alert.present();

    }

    async withdraw(id, status) {

        const alert = await this.alertController.create({
            header: 'Confirm',
            message: 'What you would like to do?',
            buttons: [
                {
                    text: 'Delete (Forever)',
                    role: 'button',
                    cssClass: 'secondary',
                    handler: async (blah) => {
                        await this.globalService.showLoader();
                        this.globalService.updateProduct(id + '?status=cancelled').subscribe(async (res) => {
                            if (res.status === AppConstant.CODE.SUCCESS) {
                                await this.globalService.closeLoader();
                                await this.getProductList();
                            } else {
                                await this.globalService.closeLoader();
                            }
                        }, async (err) => {
                            await this.globalService.closeLoader();
                            console.log(err);
                            const aler = await this.alertController.create({
                                subHeader: 'Oops!',
                                message: err.message ? err.message : 'Internal server error',
                                buttons: ['OK'],
                                // cssClass: 'bg-red'
                            });
                            await aler.present();
                        });
                    }
                }, {
                    role: 'button',
                    text: status === 'in-progress' ? 'De-List' : 'Re-List',
                    handler: async () => {
                        await this.globalService.showLoader();
                        this.globalService.updateProduct(id + (status === 'in-progress' ? '?status=suspended' : '?status=in-progress')).subscribe(async (res) => {
                            if (res.status === AppConstant.CODE.SUCCESS) {
                                await this.globalService.closeLoader();
                                await this.getProductList();
                            } else {
                                await this.globalService.closeLoader();
                            }
                        }, async (err) => {
                            await this.globalService.closeLoader();
                            console.log(err);
                            const aler = await this.alertController.create({
                                subHeader: 'Oops!',
                                message: err.message ? err.message : 'Internal server error',
                                buttons: ['OK'],
                                // cssClass: 'bg-red'
                            });
                            await aler.present();
                        });
                    }
                }
            ]
        });

        await alert.present();

    }
}
