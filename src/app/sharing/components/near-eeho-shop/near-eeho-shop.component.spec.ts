import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearEehoShopComponent } from './near-eeho-shop.component';

describe('NearEehoShopComponent', () => {
  let component: NearEehoShopComponent;
  let fixture: ComponentFixture<NearEehoShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NearEehoShopComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearEehoShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
