// import { Component, OnInit } from '@angular/core';
import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {NavParams, IonContent, Events, AlertController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Camera} from '@ionic-native/camera/ngx';
import {GlobalService} from '../../../_core/services/global.service';
import {FileTransferObject, FileUploadOptions} from '@ionic-native/file-transfer/ngx';
import {AppConstant} from '../../../_core/services/app.constant';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-terms-n-condition',
    templateUrl: './need-help.component.html',
    styleUrls: ['./need-help.component.scss'],
})

export class NeedHelpComponent implements OnInit {
    helpSubmitted = false;
    helpForm: FormGroup;
    user: any;

    constructor(private storage: Storage, private formBuilder: FormBuilder, private appService: GlobalService,
                private alertController: AlertController) {
    }

    get df() {
        return this.helpForm.controls;
    }

    ngOnInit() {
        this.helpForm = this.formBuilder.group({
            title: ['', Validators.required],
            description: ['', Validators.required]
        });
    }

    async submitHelp() {
        this.user = await this.storage.get('user');
        this.helpSubmitted = true;
        if (this.helpForm.invalid) {
            return;
        }
        await this.appService.showLoader();

        const reqObj = {
            subject: this.df.title.value,
            description: this.df.description.value,
            userId: this.user._id
        };

        this.appService.needHelp(reqObj).subscribe(async (respo) => {
            await this.appService.closeLoader();
            this.helpSubmitted = false;
            this.helpForm.reset();
            const alert = await this.alertController.create({
                subHeader: 'Success!',
                message: respo.message,
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
            this.df.expiryTime.setValue('-');
        }, async (err) => {
            await this.appService.closeLoader();
            this.helpForm.reset();
            this.helpSubmitted = false;
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });

            await alert.present();
        });
    }
}

