import {Component, OnInit} from '@angular/core';
import {ModalController, NavController, ToastController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {GlobalService} from '../../../_core/services/global.service';
import {AppConstant} from '../../../_core/services/app.constant';
import * as moment from 'moment';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {
    user: any;
    notificationList = [];
    moment: any;

    constructor(private modalCtrl: ModalController, private storage: Storage, private globalService: GlobalService,
                private toastController: ToastController, private navCtrl: NavController) {
    }

    async ngOnInit() {
        this.user = await this.storage.get('user');
        await this.getNotification();
    }

    async navigate(c) {
        const data = JSON.parse(c.data);
        if (data.notificationType && data.notificationType === 'chat-message') {
            setTimeout(async () => {
                await this.navCtrl.navigateForward(`/chat/${data.senderId}`);
            }, 100);

        } else if (data.notificationType && data.notificationType === 'request-listing') {
            setTimeout(async () => {
                await this.navCtrl.navigateForward(`/chat/${data.senderId}`);
            }, 100);

        } else if (data.notificationType && data.notificationType === 'listing') {
            console.log(`/sharing/detail/${data.productId}`);
            setTimeout(async () => {
                await this.navCtrl.navigateForward(`/sharing/detail/${data.productId}`);
            }, 100);
        }
        if (!c.isRead) {
            await Promise.all([this.markRead(c._id), this.closeModal()]);
        } else {
            await this.closeModal();
        }
    }

    async getNotification() {
        await this.globalService.showLoader();
        this.globalService.getNotification(this.user._id).subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                await this.globalService.closeLoader();
                this.notificationList = res.responseData.map((obj) => {
                    obj.addedOn = moment(obj.addedOn).fromNow();
                    return obj;
                });

            }
        }, async (err) => {
            await this.globalService.closeLoader();
            const toast = await this.toastController.create({
                message: err.message,
                duration: 3000
            });
            await toast.present();
        });

    }

    async markRead(notificationId) {
        await this.globalService.showLoader();
        this.globalService.markReadNotification(notificationId).subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                await this.globalService.closeLoader();
                this.notificationList = res.responseData.map((obj) => {
                    obj.addedOn = moment(obj.addedOn).fromNow();
                    return obj;
                });

            }
        }, async (err) => {
            await this.globalService.closeLoader();
            const toast = await this.toastController.create({
                message: err.message,
                duration: 3000
            });
            await toast.present();
        });

    }

    async closeModal() {
        await this.modalCtrl.dismiss();
    }
}
