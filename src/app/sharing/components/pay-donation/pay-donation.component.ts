import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ModalController, NavController, ToastController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {Storage} from '@ionic/storage';
import {DomSanitizer} from '@angular/platform-browser';
import {GlobalService} from '../../../_core/services/global.service';
import {AppConstant} from '../../../_core/services/app.constant';
import {YoutubeVideoPlayer} from '@ionic-native/youtube-video-player/ngx';

declare var RazorpayCheckout: any;

@Component({
    selector: 'app-pay-donation',
    templateUrl: './pay-donation.component.html',
    styleUrls: ['./pay-donation.component.scss'],
})
export class PayDonationComponent implements OnInit {

    donation: any;
    donationId: any;
    user: any;
    amount: number;

    constructor(private youtube: YoutubeVideoPlayer, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private globalService: GlobalService, private navCtrl: NavController,
                private modalCtrl: ModalController, private storage: Storage, private toastController: ToastController, private sanitizer: DomSanitizer) {
        // this.activatedRoute.queryParams.subscribe(params => {
        //     // @ts-ignore
        //     this.donation.donationId = params.donationId;
        //     // // @ts-ignore
        //     // this.donation.name = params.name;
        //     // // @ts-ignore
        //     // this.donation.age = params.age;
        //     // // @ts-ignore
        //     // this.donation.description = params.description;
        //     // // @ts-ignore
        //     // this.donation.type = params.type.toUpperCase();
        //     // // @ts-ignore
        //     // this.donation.videoUrl = params.videoUrl.replace('watch?v=', 'embed/');
        //     // // @ts-ignore
        //     // this.donation.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.donation.videoUrl);
        //     // // @ts-ignore
        //     // this.donation.documentUrl = params.documentUrl;
        //
        // });
    }

    async ngOnInit() {
    }

    play() {
        this.youtube.openVideo(this.donation.videoUrl.split('/').reverse()[0]);
    }

    async ionViewWillEnter() {
        console.log(this.donation);
        this.user = await this.storage.get('user');
        this.donationId = this.activatedRoute.snapshot.queryParams.donationId;
        await this.getDonation(this.donationId);
    }

    async getDonation(donationId) {
        await this.globalService.showLoader();
        this.globalService.getDonation(donationId).subscribe(async (response) => {
            if (response.status === AppConstant.CODE.SUCCESS) {
                this.donation = response.responseData;
                await this.globalService.closeLoader();
                const toast = await this.toastController.create({
                    message: response.message,
                    duration: 3000
                });
                // await toast.present();
            }
        }, async (err) => {
            await this.globalService.closeLoader();
            const toast = await this.toastController.create({
                message: err.message,
                duration: 3000
            });
            await toast.present();
        });
    }

    async closeModal() {
        await this.modalCtrl.dismiss();
    }

    async donate() {
        if (!this.amount) {
            const toast = await this.toastController.create({
                message: 'Amount is empty!',
                duration: 3000
            });
            await toast.present();
            return;
        }
        // @ts-ignore
        const options = {
            description: `Donation for ${this.donation.name}`,
            image: 'https://eeho.s3.us-east-2.amazonaws.com/app/EEHO_PNG.png',
            currency: 'INR', // your 3 letter currency code
            key: 'rzp_test_1DP5mmOlF5G5ag', // your Key Id from Razorpay dashboard
            amount: this.amount * 100, // Payment amount in smallest denomiation e.g. cents for USD
            name: 'Donation',
            retry: 0,
            prefill: {
                email: this.user.email,
                contact: this.user.phone,
                name: this.user.name
            },
            theme: {
                color: '#00c06d'
            },
            modal: {}
        };

        const successCallback = (paymentId) => {
            const reqObj = {
                donor: this.user._id,
                donationId: this.donation._id,
                amount: this.amount,
                status: 'success',
                paymentId
            };
            this.amount = 0;
            this.globalService.addPaymentStatus(reqObj).subscribe(async (res) => {
                this.amount = 0;
                const toast = await this.toastController.create({
                    message: 'Shared Successfully',
                    duration: 2000
                });
                await toast.present();
                await this.navCtrl.navigateForward(['/sharing/health']);
            }, async (err) => {
                this.amount = 0;
                const toast = await this.toastController.create({
                    message: 'Shared Successfully',
                    duration: 2000
                });
                await toast.present();
                await this.navCtrl.navigateForward(['/sharing/health']);

            });
        };

        const cancelCallback = (error) => {
            const reqObj = {
                donor: this.user._id,
                donationId: this.donation.donationId,
                amount: this.amount,
                status: 'failure',
            };
            this.amount = 0;
            this.globalService.addPaymentStatus(reqObj).subscribe(async (res) => {
                this.amount = 0;
                const toast = await this.toastController.create({
                    message: 'Payment Failed',
                    duration: 2000
                });
                await toast.present();
                await this.navCtrl.navigateForward(['/sharing/health']);

            }, async (err) => {
                this.amount = 0;
                const toast = await this.toastController.create({
                    message: 'Payment Failed',
                    duration: 2000
                });
                await toast.present();
                await this.navCtrl.navigateForward(['/sharing/health']);

            });
        };


        RazorpayCheckout.open(options, successCallback, cancelCallback);

    }
}
