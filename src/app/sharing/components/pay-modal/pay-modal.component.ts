import {Component, OnInit} from '@angular/core';
import {ModalController, ToastController} from '@ionic/angular';
import {SafeResourceUrl} from '@angular/platform-browser';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../../../_core/services/global.service';
import {AppConstant} from '../../../_core/services/app.constant';
import {Storage} from '@ionic/storage';
import {FileTransfer, FileTransferObject, FileUploadOptions} from '@ionic-native/file-transfer/ngx';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';

@Component({
    selector: 'app-pay-modal',
    templateUrl: './pay-modal.component.html',
    styleUrls: ['./pay-modal.component.scss'],
})
export class PayModalComponent implements OnInit {
    public url1: SafeResourceUrl;
    private fileTransfer: FileTransferObject;
    donateSubmitted = false;
    donateForm: FormGroup;
    user: any;
    imageData: any;
    profileUrl: string;

    constructor(private webView: WebView, private formBuilder: FormBuilder,
                private modalCtrl: ModalController, private transfer: FileTransfer,
                private camera: Camera, private storage: Storage, private service: GlobalService,
                private toastController: ToastController) {
    }

    async ngOnInit() {
        this.user = await this.storage.get('user');
        this.donateForm = this.formBuilder.group({
            donationType: ['', Validators.required],
            description: ['', Validators.required],
            name: ['', Validators.required],
            age: ['', [Validators.required, Validators.max(100)]],
            videoUrl: ['', [Validators.required]],
            targetAmount: ['', [Validators.required]],
            documentUrl: ['']
        });
    }

    async closeModal() {
        await this.modalCtrl.dismiss();
    }

    get df() {
        return this.donateForm.controls;
    }

    uploadOrTakePhoto() {
        const options: CameraOptions = {
            quality: 100,
            targetHeight: 650,
            targetWidth: 650,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
        this.camera.getPicture(options).then((imageData) => {
            this.imageData = imageData;
            const currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
            this.profileUrl = this.webView.convertFileSrc(imageData);
            // this.isUpdateClicked = true;
            this.fileTransfer = this.transfer.create();

        }, (err) => {
            // Handle error
        });

    }

    async addDonation() {
        this.donateSubmitted = true;
        if (this.donateForm.invalid) {
            return;
        }
        const options: FileUploadOptions = {
            chunkedMode: false,
            fileName: this.imageData.substr(this.imageData.lastIndexOf('/') + 1)
        };
        const reqObj = {
            type: this.df.donationType.value,
            description: this.df.description.value,
            name: this.df.name.value,
            age: this.df.age.value,
            videoUrl: this.df.videoUrl.value.replace('watch?v=', 'embed/'),
            addedBy: this.user._id,
            documentUrl: this.df.documentUrl.value,
            targetAmount: this.df.targetAmount.value,
        };
        this.fileTransfer.upload(this.imageData, AppConstant.BASE_URL + AppConstant.UPLOAD_MEDIA
            , options, true).then(async (res) => {
            console.log('file uploaded successfully.', res);
            const responseObj = JSON.parse(res.response);
            // @ts-ignore
            reqObj.profileUrl = responseObj.responseData.mediaUrl;

            await this.service.showLoader();
            this.service.addDonation(reqObj).subscribe(async (response) => {
                if (response.status === AppConstant.CODE.SUCCESS) {
                    this.donateSubmitted = false;
                    this.profileUrl = '';
                    await this.service.closeLoader();
                    const toast = await this.toastController.create({
                        message: response.message,
                        duration: 3000
                    });
                    await toast.present();
                    this.donateForm.reset();
                    await this.closeModal();
                }
            }, async (err) => {
                this.donateSubmitted = false;
                await this.service.closeLoader();
                this.profileUrl = '';
                const toast = await this.toastController.create({
                    message: err.message,
                    duration: 3000
                });
                await toast.present();
                this.donateForm.reset();
            });
        });
    }
}
