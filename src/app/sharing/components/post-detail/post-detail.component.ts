import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {AlertController, NavController, ToastController} from '@ionic/angular';
import {GlobalService} from '../../../_core/services/global.service';
import {AppConstant} from '../../../_core/services/app.constant';
import {ProductModel} from '../../../_core/data/product.model';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-post-detail',
    templateUrl: './post-detail.component.html',
    styleUrls: ['./post-detail.component.scss'],
})
export class PostDetailComponent implements OnInit {

    productId = '';
    product: ProductModel;
    relatedProduct: ProductModel[];
    rating = 0;

    user: any;
    slideOpts = {
        initialSlide: 1,
        speed: 400
    };

    constructor(private storage: Storage, private alertController: AlertController, protected toastController: ToastController,
                private activatedRoute: ActivatedRoute, private navCtrl: NavController, public router: Router,
                private globalService: GlobalService) {
    }

    async ngOnInit() {

    }

    async ionViewWillEnter() {

        this.user = await this.storage.get('user');
        this.productId = this.activatedRoute.snapshot.paramMap.get('productId');
        console.log(this.productId);
        await this.getProductDetail();

    }

    async getRelatedProduct() {
        this.globalService.getRelatedProduct(this.productId, this.product.category).subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                this.relatedProduct = res.responseData;
            }
        }, async (err) => {
            console.log(err);
            const aler = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await aler.present();
        });
    }

    async getProductDetail() {
        await this.globalService.showLoader();
        this.globalService.getProduct(this.productId).subscribe(async (res) => {
            await this.globalService.closeLoader();
            if (res.status === AppConstant.CODE.SUCCESS) {
                this.product = new ProductModel(res.responseData);
                await this.getRelatedProduct();
            }
        }, async (err) => {
            await this.globalService.closeLoader();
            console.log(err);
            const aler = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await aler.present();
        });
    }

    async navigateToHome() {
        await this.navCtrl.navigateBack('/sharing/home');
    }

    async requestListing() {
        const alert = await this.alertController.create({
            header: 'Confirm',
            message: 'Are you sure?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                }, {

                    text: 'Okay',
                    handler: async () => {
                        await this.globalService.showLoader();
                        this.globalService.requestListing(this.productId).subscribe(async (res) => {
                            if (res.status === AppConstant.CODE.SUCCESS) {
                                await this.globalService.closeLoader();
                                const aler = await this.alertController.create({
                                    subHeader: 'Success!',
                                    message: res.message,
                                    buttons: ['OK'],
                                    // cssClass: 'bg-red'
                                });
                                await aler.present();
                            } else {
                                await this.globalService.closeLoader();
                            }
                        }, async (err) => {
                            await this.globalService.closeLoader();
                            console.log(err);
                            const aler = await this.alertController.create({
                                subHeader: 'Oops!',
                                message: err.message ? err.message : 'Internal server error',
                                buttons: ['OK'],
                                // cssClass: 'bg-red'
                            });
                            await aler.present();
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    likeProduct() {
        this.globalService.likeProduct(this.productId).subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                await this.globalService.closeLoader();
                await this.getProductDetail();
            } else {
                await this.globalService.closeLoader();
            }
        }, async (err) => {
            await this.globalService.closeLoader();
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }

    logRatingChange(rating) {
        console.log('changed rating: ', rating);
        // do your stuff
        this.rating = rating;
    }

    async rateProduct() {

        if (this.product.selfRating > 0) {
            const toast = await this.toastController.create({
                message: 'Already rated',
                duration: 2000
            });
            await toast.present();
            return;
        }
        if (!this.rating) {
            const toast = await this.toastController.create({
                message: 'Please rate first!',
                duration: 2000
            });
            await toast.present();
            return;
        }
        await this.globalService.showLoader();
        const obj = {
            productId: this.product.productId,
            value: this.rating
        };
        this.globalService.rateProduct(obj).subscribe(async (res) => {
            await this.globalService.closeLoader();
            this.getProductDetail();
        }, async (err) => {
            await this.globalService.closeLoader();
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message,
                buttons: ['OK'],
            });
            await alert.present();
        });
    }

    async navigateToDetail(product) {
        await this.navCtrl.navigateForward(`/sharing/detail/${product}`);
    }
    async navigateToProfile(user) {
        const navigationExtras: NavigationExtras = {
            state: {
                user,
                backRoute: this.router.url
            },
            replaceUrl: true
        };
        await this.navCtrl.navigateForward([`/sharing/public-profile`], navigationExtras);
    }

}
