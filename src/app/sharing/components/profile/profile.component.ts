import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Storage} from '@ionic/storage';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {GlobalService} from '../../../_core/services/global.service';
import {Events, NavController, Platform} from '@ionic/angular';
import {File, FileEntry} from '@ionic-native/file/ngx';
import {FileTransfer, FileUploadOptions, FileTransferObject} from '@ionic-native/file-transfer/ngx';
import {AppConstant} from '../../../_core/services/app.constant';
import {WebView} from '@ionic-native/ionic-webview/ngx';


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

    user: any;
    submitted = false;
    profileForm: FormGroup;
    private fileTransfer: FileTransferObject;
    profileUrl: string;
    imageData: any;
    isUpdateClicked = false;


    constructor(public event: Events, private webView: WebView, private ref: ChangeDetectorRef,
                private transfer: FileTransfer, private file: File, private platform: Platform,
                private globalService: GlobalService, private storage: Storage,
                private formBuilder: FormBuilder, private camera: Camera, private navCtrl: NavController) {
    }

    async ngOnInit() {

    }

    async ionViewWillEnter() {
        this.user = await this.storage.get('user');
        this.globalService.currentMessage.subscribe((message) => {
            if (message) {
                console.log(message);
                this.user = message;
            }
        });
        this.profileForm = this.formBuilder.group({
            // firstName: [this.user.firstName, Validators.required],
            radius: [this.user.radius / 1000, Validators.required],
            email: [{value: this.user.email, disabled: true}, Validators.required],
            name: [this.user.name, Validators.required],
            address: [this.user.address, Validators.required],
            phone: [this.user.phone, [Validators.required, Validators.pattern('^[0-9]{10}$')]],
            zipCode: [this.user.zipCode, [Validators.required, Validators.maxLength(6), Validators.pattern('^[0-9]{6}$')]],
        });
        this.platform.ready().then((res) => {
            this.fileTransfer = this.transfer.create();
        });
    }


    async verifyPhone() {
        await this.navCtrl.navigateForward('/landing/verify-otp', {queryParams: {phone: this.user.phone, route: 'profile'}});

    }

    get f() {
        return this.profileForm.controls;
    }

    uploadOrTakePhoto() {

        const options: CameraOptions = {
            quality: 100,
            targetHeight: 650,
            targetWidth: 650,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
        this.camera.getPicture(options).then((imageData) => {
            this.imageData = imageData;
            const currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
            this.profileUrl = this.webView.convertFileSrc(imageData);
            this.isUpdateClicked = true;
            this.ref.detectChanges();


        }, (err) => {

            // Handle error
        });

    }

    async saveChanges() {

        this.submitted = true;
        if (this.user.phone && this.user.isVerified === 1) {
            this.f.phone.clearValidators();
            this.f.phone.setErrors(null);
        }
        if (this.profileForm.invalid) {
            return;
        }
        if (this.isUpdateClicked) {
            await this.globalService.showLoader();
            console.log(this.imageData.substr(this.imageData.lastIndexOf('/') + 1));
            const options: FileUploadOptions = {
                chunkedMode: false,
                fileName: this.imageData.substr(this.imageData.lastIndexOf('/') + 1)
            };
            this.fileTransfer.upload(this.imageData, AppConstant.BASE_URL + AppConstant.UPLOAD_MEDIA
                , options, true).then((res) => {
                console.log('file uploaded successfully.', res);
                const responseObj = JSON.parse(res.response);
                const reQObj = {
                    profileUrl: responseObj.responseData.mediaUrl,
                    userId: this.user._id,
                    name: this.f.name.value ? this.f.name.value : this.user.name,
                    address: this.f.address.value ? this.f.address.value : this.user.address,
                    zipCode: this.f.zipCode.value ? this.f.zipCode.value : this.user.zipCode,
                };
                if (this.user.phone !== this.f.phone.value) {
                    // @ts-ignore
                    reQObj.phone = this.f.phone.value ? '+91' + this.f.phone.value : this.user.phone;

                }
                this.globalService.updateUser(reQObj).subscribe(async (responseUpdate) => {
                    console.log(responseUpdate);
                    this.submitted = false;
                    this.user = responseUpdate.responseData;
                    await this.storage.set('user', responseUpdate.responseData);
                    this.globalService.changeMessage(responseUpdate.responseData);
                    this.ref.detectChanges();
                    await this.globalService.closeLoader();
                });
            }).catch(async (error) => {
                this.submitted = false;
                // here logging an error.
                console.log('upload failed: ' + JSON.stringify(error));
                await this.globalService.closeLoader();
            });
        } else {
            const reqObj = {
                userId: this.user._id,
                name: this.f.name.value ? this.f.name.value : this.user.name,
                address: this.f.address.value ? this.f.address.value : this.user.address,
                zipCode: this.f.zipCode.value ? this.f.zipCode.value : this.user.zipCode,
            };
            if (this.user.phone !== this.f.phone.value) {
                // @ts-ignore
                reqObj.phone = this.f.phone.value ? '+91' + this.f.phone.value : this.user.phone;
            }
            if (this.user.radius !== this.f.radius.value) {
                // @ts-ignore
                reqObj.radius = this.f.radius.value;
            }
            await this.globalService.showLoader();
            this.globalService.updateUser(reqObj).subscribe(async (responseUpdate) => {
                console.log(responseUpdate);
                this.submitted = false;
                await this.storage.set('user', responseUpdate.responseData);
                this.user = responseUpdate.responseData;
                this.globalService.changeMessage(responseUpdate.responseData);
                this.ref.detectChanges();
                await this.globalService.closeLoader();
            }, (async error => {
                this.submitted = false;
                await this.globalService.closeLoader();
            }));
        }
    }

}
