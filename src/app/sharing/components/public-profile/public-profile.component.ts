import {Component, OnInit} from '@angular/core';
import {NavController, ToastController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {GlobalService} from '../../../_core/services/global.service';
import {map} from 'rxjs/operators';
import {Storage} from '@ionic/storage';
import {AppConstant} from '../../../_core/services/app.constant';

@Component({
    selector: 'app-public-profile',
    templateUrl: './public-profile.component.html',
    styleUrls: ['./public-profile.component.scss'],
})
export class PublicProfileComponent implements OnInit {

    user: any;
    currentUser: any;
    chatId: string;
    backRoute: string;
    slideOpts = {
        initialSlide: 1,
        speed: 400,
    };
    postedProduct = [];
    rating = 0;
    status = -1;
    message = '';

    constructor(private navCtrl: NavController, private toastController: ToastController, private route: ActivatedRoute,
                private router: Router, private globalService: GlobalService, private storage: Storage,) {
    }

    async ngOnInit() {
    }

    async routeBack() {
        await this.navCtrl.navigateBack([this.backRoute]);
    }

    async navigateToDetail(product) {
        await this.navCtrl.navigateForward(`/sharing/detail/${product}`);
    }

    async navigateToChat() {
        // earlier flow was to navigate to chat window but now need to send request and if approved then

        // await this.navCtrl.navigateForward([`/chat/${this.chatId}`], {replaceUrl: true});
        if (this.status === -1) {
            this.globalService.sendChatRequest({senderId: this.currentUser._id, receiverId: this.user._id}).subscribe(async (res: any) => {
                await this.getChatStatus();
                const toast = await this.toastController.create({
                    message: 'Chat request sent!',
                    duration: 3000
                });
                await toast.present();
            }, async () => {
                const toast = await this.toastController.create({
                    message: 'Something went wrong! Try again',
                    duration: 3000
                });
                await toast.present();
            });
        } else if (this.status === 1) {
            await this.navCtrl.navigateForward([`/chat/${this.chatId}`], {replaceUrl: true});
        } else if (this.status === 0 && this.message.length) {
            await this.navCtrl.navigateForward([`/sharing/message`], {replaceUrl: true});
        } else {
            return;
        }

    }

    async ionViewWillEnter() {
        await this.globalService.showLoader();
        let count = 0;
        this.currentUser = await this.storage.get('user');
        this.route.queryParams.subscribe(async params => {
            this.route.paramMap.pipe(map(() => window.history.state)).subscribe(async (obj) => {
                this.backRoute = obj.backRoute;
                if (obj && obj.user) {
                    this.user = obj.user;
                    if (this.user.addedBy && count === 0) {
                        count = 1;
                        await this.getPostedProduct(this.user.addedBy);
                        await this.getChatStatus();
                        this.chatId = this.user.addedBy;
                    } else if (this.user._id && count === 0) {
                        count = 1;
                        await this.getPostedProduct(this.user._id);
                        await this.getChatStatus();
                        this.chatId = this.user._id;
                    }
                    console.log(this.user);
                    await this.globalService.closeLoader();
                } else {
                    await this.globalService.closeLoader();
                    const toast = await this.toastController.create({
                        message: 'Something went wrong! Try again',
                        duration: 3000
                    });
                    // await toast.present();
                    await this.navCtrl.navigateBack(['/sharing/home']);
                }
            }, async () => {
                await this.globalService.closeLoader();
                const toast = await this.toastController.create({
                    message: 'Something went wrong! Try again',
                    duration: 3000
                });
                // await toast.present();
            });
        });
    }

    async getPostedProduct(userId) {


        if (!userId) {
            const toast = await this.toastController.create({
                message: 'Something went wrong! Try again',
                duration: 3000
            });
            // await toast.present();
        }
        this.globalService.getProductByUserId({
            addedBy: userId,
            productId: this.user.productId ? this.user.productId : 'product'
        }).subscribe((res: any) => {
            this.postedProduct = res.responseData.product;
            this.rating = res.responseData.rating;
        }, async () => {
            const toast = await this.toastController.create({
                message: 'Something went wrong! Try again',
                duration: 3000
            });
            // await toast.present();
        });
    }

    private async getChatStatus() {
        this.globalService.getChatRequestStatus({
            senderId: this.user._id,
            receiverId: this.currentUser._id
        }).subscribe((res: any) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                this.status = res.responseData.status;
                this.message = res.responseData.message;
            }
        }, async () => {
            const toast = await this.toastController.create({
                message: 'Something went wrong! Try again',
                duration: 3000
            });
            // await toast.present();
        });
    }
}
