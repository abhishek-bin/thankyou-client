import {Component, OnInit, SecurityContext} from '@angular/core';
import {GlobalService} from '../../../_core/services/global.service';
import {DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';

@Component({
    selector: 'app-terms',
    templateUrl: './terms.component.html',
    styleUrls: ['./terms.component.scss'],
})
export class TermsComponent implements OnInit {

    url: SafeResourceUrl;

    constructor(public service: GlobalService, private sanitizer: DomSanitizer) {
    }

    async ngOnInit() {
        setTimeout(async () => {
            await this.service.showLoader();
            this.url = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.eeho.app/tnc');
        }, 1000);
    }

    async called(e) {
        console.log(e);
        await this.service.closeLoader();

    }
}
