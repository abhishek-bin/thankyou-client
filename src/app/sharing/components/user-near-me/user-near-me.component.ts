import {Component, OnInit} from '@angular/core';
import {GlobalService} from '../../../_core/services/global.service';
import {AppConstant} from '../../../_core/services/app.constant';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {Storage} from '@ionic/storage';
import {AlertController, NavController} from '@ionic/angular';
import {NavigationExtras, Router} from '@angular/router';

@Component({
    // selector: 'app-ui',
    selector: 'app-user-near-me',
    templateUrl: './user-near-me.component.html',
    styleUrls: ['./user-near-me.component.scss'],
})
export class UserNearMeComponent implements OnInit {
    private userObj: any;

    constructor(private alertController: AlertController, private navCtrl: NavController, private storage: Storage, private globalService: GlobalService, private geolocation: Geolocation, public router: Router,) {
    }

    tempUser = [];
    userList = [];
    filterValue = '';

    async ngOnInit() {

    }

    async ionViewWillEnter() {
        this.userObj = await this.storage.get('user');
        await this.getUsersNearMe();
    }


    async getUsersNearMe() {
        await this.globalService.showLoader();
        this.geolocation.getCurrentPosition().then((resp) => {
            const location = `${resp.coords.longitude},${resp.coords.latitude}`;
            this.globalService.getUsersNearMe(location, this.userObj._id).subscribe((res) => {
                this.globalService.closeLoader();
                if (res.status === AppConstant.CODE.SUCCESS) {
                    this.userList = res.responseData;
                    this.tempUser = res.responseData;
                }
            });
        }, async (err) => {
            await this.globalService.closeLoader();
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });

            await alert.present();
        });
    }

    async getDistance(ev) {
        if (ev.target.value) {
            this.filterUserRecords(ev.target.value);
        } else {
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: 'Select filter option first',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });

            await alert.present();
            return;
        }

    }

    private filterUserRecords(value: any) {
        this.userList = [...this.tempUser];
        this.userList = this.tempUser.filter(o => o.distance <= value);
    }

    reset() {
        this.filterValue = null;
        this.userList = [...this.tempUser];
    }

    async navigate(user) {
        const navigationExtras: NavigationExtras = {
            state: {
                user,
                backRoute: this.router.url
            }
        };
        await this.navCtrl.navigateForward([`/sharing/public-profile`], navigationExtras);
    }
}

