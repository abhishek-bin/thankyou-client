import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AlertController, NavController, ToastController} from '@ionic/angular';
import {GlobalService} from '../../../_core/services/global.service';
import {AppConstant} from '../../../_core/services/app.constant';
import * as moment from 'moment';
import {Storage} from '@ionic/storage';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';

@Component({
    selector: 'app-verify-otp',
    templateUrl: './verify-otp.component.html',
    styleUrls: ['./verify-otp.component.scss'],
})
export class VerifyOtpComponent implements OnInit {

    phone = '';
    otp = '';
    route = '';
    isOtpSent = 0;

    constructor(public toastController: ToastController, public fcm: FirebaseX, private storage: Storage, private alertController: AlertController, private router: ActivatedRoute, private nav: NavController, private service: GlobalService
    ) {
    }

    ngOnInit() {
        this.phone = this.router.snapshot.queryParams.phone;
        this.route = this.router.snapshot.queryParams.route;
        this.sendOtp();
    }

    async sendOtp() {
        this.isOtpSent = 1;
        if (!this.phone) {
            await this.nav.navigateRoot('/landing/');
            return;
        }
        this.service.sendOtp(this.phone).subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                const toast = await this.toastController.create({
                    message: 'OTP sent successfully',
                    duration: 2000
                });
                await toast.present();
                await this.service.closeLoader();
            } else {
                await this.service.closeLoader();
            }
        }, async (err) => {
            await this.service.closeLoader();
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }

    verifyOtp() {
        if (!this.otp) {
            this.alertController.create({
                subHeader: '',
                message: 'Enter 6 digit OTP',
                buttons: ['OK']
            });
            return;
        }
        const req = {
            otp: this.otp,
            phone: this.phone
        };
        this.service.verifyOtp(req).subscribe(async (res) => {
            if (res.status === AppConstant.CODE.SUCCESS) {
                await this.service.closeLoader();
                await this.storage.clear();
                await this.storage.set('user', res.responseData);
                await this.storage.set('token', res.responseData.token);
                this.service.changeMessage(res.responseData);
                this.fcm.getToken().then((token) => {
                    this.service.saveDeviceToken({userId: res.responseData._id, deviceToken: token}).subscribe((r) => {
                    });
                });
                if (this.route === 'profile') {
                    await this.nav.navigateForward(['sharing/profile'], {replaceUrl: true});
                } else {
                    await this.nav.navigateForward(['sharing/home'], {replaceUrl: true});
                }
            } else {
                await this.service.closeLoader();
            }
        }, async (err) => {
            await this.service.closeLoader();
            console.log(err);
            const alert = await this.alertController.create({
                subHeader: 'Oops!',
                message: err.message ? err.message : 'Internal server error',
                buttons: ['OK'],
                // cssClass: 'bg-red'
            });
            await alert.present();
        });
    }

}
