import { TestBed } from '@angular/core/testing';

import { TabSelectionsService } from './tab-selections.service';

describe('TabSelectionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TabSelectionsService = TestBed.get(TabSelectionsService);
    expect(service).toBeTruthy();
  });
});
