import {Injectable} from '@angular/core';

import {BehaviorSubject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TabSelectionsService {
    private tabSelectedBar: any = new BehaviorSubject(null);
    private tabSelected: any = new BehaviorSubject(null);
    $tabSelected = this.tabSelected.asObservable();
    $tabSelectedBar = this.tabSelectedBar.asObservable();

    updateTabSelected(selection: string) {
        this.tabSelected.next(selection);
    }
    updateTabSelectedBar(selection: string, id: number) {
        this.tabSelectedBar.next({selection, id});
    }

    constructor() {
    }
}
