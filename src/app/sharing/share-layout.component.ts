import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HeaderComponent } from './../_core/components/header/header.component';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-share-layout',
  templateUrl: './share-layout.component.html',
  styleUrls: ['./share-layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShareLayoutComponent implements OnInit {

  constructor(private navCtrl: NavController) {
   }

  ngOnInit() {}

}
