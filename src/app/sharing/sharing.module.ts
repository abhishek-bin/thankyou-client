import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ShareLayoutComponent} from './share-layout.component';
import {HomeComponent} from './components/home/home.component';
import {SharedModule} from '../_core/modules/shared.modules';
import {CreateListingComponent} from './components/create-listing/create-listing.component';
import {UserNearMeComponent} from './components/user-near-me/user-near-me.component';
// import {HealthEduComponent} from './components/health-edu/health-edu.component';
import {FeedbackComponent} from './components/feedback/feedback.component';
import {GuidelinesComponent} from './components/guidelines/guidelines.component';
import {NeedHelpComponent} from './components/need-help/need-help.component';
import {NearEehoShopComponent} from './components/near-eeho-shop/near-eeho-shop.component';
import {MessageComponent} from './components/message/message.component';
import {ProfileComponent} from './components/profile/profile.component';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PostDetailComponent} from './components/post-detail/post-detail.component';
import {Camera} from '@ionic-native/camera/ngx';
import {AppSettingComponent} from './components/app-setting/app-setting.component';
import {ForumDetailComponent} from './components/forum-detail/forum-detail.component';
import {ForumComponent} from './components/forum/forum.component';
import {ChatComponent} from './components/chat/chat.component';
import {FileTransfer, FileUploadOptions, FileTransferObject} from '@ionic-native/file-transfer/ngx';
import {File} from '@ionic-native/file/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {MyListingComponent} from './components/my-listing/my-listing.component';
import {MyOrderComponent} from './components/my-order/my-order.component';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';
import {HealtheduComponent} from './components/healthedu/healthedu.component';
import {AuthGuard} from '../_core/services/auth.guard';
import {SplashComponent} from './components/splash/splash.component';
import {ComingSoonComponent} from './components/coming-soon/coming-soon.component';
import {PayDonationComponent} from './components/pay-donation/pay-donation.component';
import {StarRatingModule} from 'ionic5-star-rating';
import {TermsComponent} from './components/terms/terms.component';
import {PublicProfileComponent} from './components/public-profile/public-profile.component';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import {InviteComponent} from './components/invite/invite.component';
import {PhotoViewer} from '@ionic-native/photo-viewer/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';


@NgModule({
    declarations: [PublicProfileComponent, TermsComponent, PayDonationComponent, HealtheduComponent, ChatComponent,
        PostDetailComponent, ProfileComponent, ShareLayoutComponent, ForumComponent, MessageComponent, CreateListingComponent,
        UserNearMeComponent, FeedbackComponent, GuidelinesComponent, NeedHelpComponent, NearEehoShopComponent, AppSettingComponent,
        ForumDetailComponent, ForumComponent, MyListingComponent, MyOrderComponent, ComingSoonComponent, SplashComponent, InviteComponent],
    imports: [SharedModule,
        RouterModule.forChild([
            {
                path: '',
                component: ShareLayoutComponent,
                children: [
                    {
                        path: 'home',
                        component: HomeComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'createListing',
                        component: CreateListingComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'my-listing',
                        component: MyListingComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'my-order',
                        component: MyOrderComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'forum',
                        component: ForumComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'forum/:forumId',
                        component: ForumDetailComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'message',
                        component: MessageComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'user-near-me',
                        component: UserNearMeComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'feedback',
                        component: FeedbackComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'guidelines',
                        component: GuidelinesComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'need-help',
                        component: NeedHelpComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'shop',
                        component: NearEehoShopComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'profile',
                        component: ProfileComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'detail/:productId',
                        component: PostDetailComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'setting',
                        component: AppSettingComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'blog-detail',
                        component: ForumDetailComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'blog',
                        component: ForumComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'chat/:receiverId',
                        component: ChatComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'health',
                        component: HealtheduComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'pay-donation',
                        component: PayDonationComponent,
                        canActivate: [AuthGuard]
                    },
                    {
                        path: 'coming-soon',
                        component: ComingSoonComponent
                    },
                    {
                        path: 'splash',
                        component: SplashComponent
                    },
                    {
                        path: 'terms',
                        component: TermsComponent
                    },
                    {
                        path: 'public-profile',
                        component: PublicProfileComponent
                    },
                    {
                        path: 'invite',
                        component: InviteComponent
                    },
                ]
            }
        ]),
        ReactiveFormsModule, CommonModule, StarRatingModule
    ],

    providers: [AuthGuard, Geolocation, Camera, FileTransfer, FileTransferObject, File, WebView, FirebaseX, SocialSharing, PhotoViewer, AndroidPermissions,


    ]
})
export class SharingModule {
}
