import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {UiComponent} from './ui.component';
import {InfoDialogComponent} from './components/info-dialog/info-dialog.component';
import {SharedModule} from '../_core/modules/shared.modules';

@NgModule({
    declarations: [UiComponent, InfoDialogComponent, ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '',
                component: UiComponent,
                children: [
                    {
                        path: 'info-dialog',
                        component: InfoDialogComponent
                    }
                ]
            }
        ])
    ]
})
export class UIModule {
}
