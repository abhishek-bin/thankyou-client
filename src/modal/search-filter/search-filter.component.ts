import {Component, OnInit} from '@angular/core';
import {Events, ModalController} from '@ionic/angular';
import {GlobalService} from '../../app/_core/services/global.service';

@Component({
    selector: 'app-search-filter',
    templateUrl: './search-filter.component.html',
    styleUrls: ['./search-filter.component.scss'],
})
export class SearchFilterComponent implements OnInit {

    type = '';
    distance = '';
    wanted = '';
    category = '';

    constructor(private globalService: GlobalService, private event: Events, private modalCtrl: ModalController) {
    }

    ngOnInit() {
        const filter = this.globalService.getCurrentFilter();
        if (filter && filter.hasOwnProperty('type')) {
            // @ts-ignore
            this.type = filter.type;
        }
        if (filter && filter.hasOwnProperty('wanted')) {
            // @ts-ignore
            this.wanted = filter.wanted;
        }
        if (filter && filter.hasOwnProperty('distance')) {
            // @ts-ignore
            this.distance = filter.distance;
        }
        if (filter && filter.hasOwnProperty('category')) {
            // @ts-ignore
            this.category = filter.category;
        }
    }

    closeModal() {
        this.modalCtrl.dismiss();
    }

    filterResult() {

        const filter = {};
        // @ts-ignore
        this.type ? filter.type = this.type : '';
        // @ts-ignore
        this.wanted ? filter.wanted = this.wanted : '';
        // @ts-ignore
        this.distance ? filter.distance = this.distance : '';
        // @ts-ignore
        this.category ? filter.category = this.category : '';
        this.event.publish('filter', filter);
        this.closeModal();
    }

    reset() {
        this.event.publish('reset-filter', {});
        this.globalService.setCurrentFilter({});
        this.closeModal();

    }

}
